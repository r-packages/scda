#require(scd)
#require(scdhlm)

# This function extracts all the variance components from a nlme object

extract_varcomp <- function(mod) {

  sigma_sq <- mod$sigma^2                                           # sigma^2
  cor_params <- as.double(stats::coef(mod$modelStruct$corStruct, FALSE))   # correlation structure
  var_params <- as.double(stats::coef(mod$modelStruct$varStruct, FALSE))   # variance structure
  Tau_params <- stats::coef(mod$modelStruct$reStruct, FALSE) * sigma_sq    # unique coefficients in Tau

  # split Tau by grouping variables
  group_names <- names(mod$groups)
  Tau_param_list <- sapply(group_names,
                           function(x) Tau_params[grep(x, names(Tau_params))],
                           simplify = FALSE, USE.NAMES = TRUE)

  varcomp <- list(Tau = Tau_param_list, cor_params = cor_params, var_params = var_params, sigma_sq=sigma_sq)

  class(varcomp) <- "varcomp"
  return(varcomp)
}


# This function computes the confidence intervals from nlme object, using the symmatric t-distribution
# See Pustejovsky et al. (2014), formula (16)
# It uses extract_varcomp

confIntES <- function(model, p = c(0,1), r=c(1,1), alpha = 0.05) {

omega <- unlist(extract_varcomp(model))
c_omega <- diag(omega)
gamma <- nlme::fixef(model)
c_gamma = stats::vcov(model)
kappa <- sqrt((p %*% c_gamma %*% p) / (r %*% omega ))

nu <- (2*(r %*% omega)^2) / (r %*% c_omega %*% r)

Jnu <- 1
if (nu > 1) Jnu <- 1 - 3/(4*nu - 1)
Jnu <- 1 - 3/(4*nu - 1)
dAB <- (p %*% gamma) / sqrt(sum(omega*r))
gAB <- Jnu * dAB

VgAB <- Jnu^2 * ((nu * kappa^2)/(nu - 2) +
                   gAB^2 * (nu/(nu - 2) - 1/Jnu^2))

tv <- stats::qt(alpha/2, nu, lower.tail = F)
CI <- c(gAB - sqrt(VgAB)*tv , gAB + sqrt(VgAB)*tv )

res <- list()
res$Jnu <- Jnu
res$CI <-  CI
res$dAB <- dAB
res$gAB <- gAB
res$VgAB <- VgAB

return(res)
}


# test functions
#confIntES(model = m1, p=p, r=c(1,0,1))



