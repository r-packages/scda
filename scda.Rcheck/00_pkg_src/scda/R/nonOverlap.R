#' nonOverlap
#'
#' This function conducts analyses of single case designs (nonOverlap).
#' The function is a wrapper, containing various different single case analysis functions.
#' These functions are copied from Manolov & Moeyaert (2017) inclusing the references.
#' Only one adaption: the required input format has been made consistent across all functions.
#'
#'
#' @param data The dataframe containing the variables for the analysis.
#' @param timeVar The name of the variable containing the measurement moments
#' (or an index of measurement moments). An index can also be specified.
#' @param yVar The name of the dependent variable. An index can also be
#' specified.
#' @param phaseVar The variable containing the phase of each measurement. Note
#' that this normally should only have two possible values. The first should indicate the pre-intervention fase.
#' @param baselineMeasurements If no phaseVar is specified,
#' \code{baselineMeasurements} can be used to specify the number of baseline
#' measurements, which is then used to construct the \code{phaseVar} dummy
#' variable.
#' @param improvement indicates whether improvements correpond to increasing values ("increase") versus ("decrease").
#'
#' @return Mainly, this functions print its results, but it also returns them
#' in an object containing three lists: \item{input}{The arguments specified
#' when calling the function} \item{intermediate}{Intermediat objects and
#' values} \item{output}{The results such as the plot.}
#'
#' @author Peter Verboon (Open University of the Netherlands)
#'
#' Maintainer: Gjalt-Jorn Peters <gjalt-jorn@@userfriendlyscience.com>
#' @seealso \code{\link{genlog}}
#'
#' @references Manolov, R. & Moeyaert, M. (2017). How can single case data be analyzed?
#' Software resources, tutorial and reflections on analysis.
#' \emph{Behavior Modification} \doi{10.1177/0145445516664307}
#' \doi{10.1177/0145445516664307}
#'
#' @keywords single case analyses
#' @rdname nonOverlap
#' @examples ### Get example data and extract data for Jason
#' data("Singh", package="scda");
#' dat <- Singh[Singh$tier==1, ];
#'
#' ### Compute non-overlap
#' scda::nonOverlap(
#'   dat,
#'   timeVar='time',
#'   yVar='score_verbal',
#'   phaseVar = NULL,
#'   baselineMeasurements=5
#' );
#'
#' @export nonOverlap
#'
nonOverlap <- function(data = NULL,  timeVar = NULL, yVar = NULL, phaseVar = NULL,
                baselineMeasurements = NULL, improvement = "increase" )
  {

  if (is.null(yVar)) {
    cat("Input error: Dependent variable (yVar) has not been specified")
    return(NULL)
  }
  if (is.null(phaseVar) & is.null(baselineMeasurements)) {
     cat("Input error: Either phaseVar or baselineMeasurements should be specified")
     return(NULL)
  }

  if (!is.null(baselineMeasurements)) {
     if (is.null(phaseVar) & (baselineMeasurements == 0)) {
        cat("Input error: Either phaseVar or baselineMeasurements > 0 should be specified")
        return(NULL)
     }
  }

  res <- list(input = as.list(environment()),
              intermediate = list(),
              output = list());


  Npre <- baselineMeasurements
  if (!is.null(data)) {  # Note. Yvar should be a character type"
    data <- data.frame(data)
    if (is.null(Npre)) Npre <- table(data[,phaseVar])[1]
    if (!is.null(Npre) & Npre == 0) Npre <- table(data[,phaseVar])[1]
    if (!is.numeric(data[,yVar])) data[,yVar] <- as.numeric(data[,yVar])
    if (is.null(timeVar)) {
      data$timeVar <- c(1:nrow(data))
      timeVar <- "timeVar"
    }
    score <- data[, yVar]
  }

  if (is.null(data)) {
    if (is.null(Npre)) {
      cat("Input error: Npre must have a value when a dateframe has not been specified")
      return(NULL)
    }
    if (is.character(yVar)) {
      cat("Input error: yVar must be a numerical vector when a dateframe has not been specified")
      return(NULL)
    }
    score <- as.numeric(yVar)
    data <- data.frame(row.names = c(1:length(score)))
    data$yVar <- score
    data$timeVar <- c(1:length(score))
    timeVar <- "timeVar"
  }

  res$intermediate$score <- score
  res$intermediate$Npre <- Npre
  res$intermediate$timeVar <- timeVar
  res$intermediate$data <- data


  ## non-overlap function



  name <- c("PND","PAND","PEM","NAP","IRD", "TAU","Tau_U")
  statistic <- rep(1,7)
  results <- data.frame(cbind(name, statistic))
  results[,1] <- as.character(results[,1])
  results[,2] <- as.numeric(results[,2])
  ## non-overlap functions
  results[1,2] <- PND(score = score, n_a = Npre, improvement = improvement)
  results[2,2] <- PAND(score = score, n_a = Npre, improvement = improvement)
  results[3,2] <- PEM(score = score, n_a = Npre, improvement = improvement)
  results[4,2] <- unlist(NAP(score = score, n_a = Npre, improvement = improvement)[1])
  results[5,2] <- IRD(score = score, n_a = Npre, improvement = improvement)
  results[6,2] <- unlist(Tau(score = score, n_a = Npre, improvement = improvement)[1])
  results[7,2] <- as.numeric(Tau_U(score = score, n_a = Npre, improvement = improvement))


  results[,2] <- round(results[,2], 3)
  res$output$results <- results

  class(res) <- "nonOverlap"
  return(res)

}

#' print.nonOverlap
#'
#' This function prints the results of the nonOverlap function
#' @method print nonOverlap
#' @param x nonOverlap object
#' @param ... not used
#' @return table with parameter estimates
#' @export
print.nonOverlap <- function(x, ...) {
  table <- x$output$results
  table[,2] <- round(table[,2], 3)
  print(table);
  return(invisible(table));
}

#' plot.nonOverlap
#'
#' This function plots the data with additional reference lines
#' @method plot nonOverlap
#' @param x nonOverlap object
#' @param ... additional parameters
#' @export
plot.nonOverlap <- function(x, ...) {

  df <- x$intermediate$data
  yVar <-  x$input$yVar
  timeVar <- x$intermediate$timeVar
  phaseVar <- x$input$phaseVar

  Npre <- x$intermediate$Npre
  Nafter <- (dim(df)[1] - Npre)
  if(is.null(phaseVar)) {
    phaseVar <- "phase"
    df$phase <- c(rep("A", Npre), rep("B",Nafter ))
  } else {
    df[,(x$input$phaseVar)] <-  as.factor(df[,(x$input$phaseVar)])
  }
  improvement <- x$input$improvement
  s <- stats::complete.cases(df[,c(yVar)])
  df <- df[s,]
  Npre <- table(df[,phaseVar])[1]  ## adjust NPre with missings

  #  phase <- df[,phaseVar]
  # plot <-   ggplot2::ggplot(data = df,
  #                           ggplot2::aes(x = df[,timeVar], y = df[,yVar], colour = phase)) +
  #   ggplot2::geom_point() + ggplot2::theme_bw() +
  #   ggplot2::geom_vline(xintercept = mean(c(df[Npre, timeVar], df[Npre + 1, timeVar]))) +
  #   ggplot2::xlab("measurements") + ggplot2::ylab(yVar)
  #

  plot <- ggplot2::ggplot(data = df,
                  ggplot2::aes_string(x = timeVar, y = yVar, colour = phaseVar)) +
    ggplot2::geom_point() + ggplot2::theme_bw() +
    ggplot2::geom_vline(xintercept = mean(c(df[Npre, timeVar], df[Npre + 1, timeVar]))) +
    ggplot2::xlab("measurements") + ggplot2::ylab(yVar)


  if (improvement == "increase") {
    plot <- plot + ggplot2::geom_hline(yintercept=max(df[1:Npre,yVar]), color = "black",
                                       size = .1, linetype = "dashed")
    plot <- plot + ggplot2::geom_hline(yintercept=min(df[(Npre+1):nrow(df),yVar]), color = "black",
                                       size = .1, linetype = "dashed")

    # plot <- plot + ggplot2::geom_ribbon(ggplot2::aes(ymin=min(df[(Npre+1):nrow(df),yVar]),
    #                                                  ymax=max(df[1:Npre,yVar])), fill = "grey70", alpha = .50)
  }
  if (improvement == "decrease") {
    plot <- plot + ggplot2::geom_hline(yintercept=min(df[1:Npre,yVar]), color = "black",
                                       size = .2, linetype = "dashed")
    plot <- plot + ggplot2::geom_hline(yintercept=max(df[(Npre+1):nrow(df),yVar]), color = "black",
                                       size = .2, linetype = "dashed")

    # plot <- plot + ggplot2::geom_ribbon(ggplot2::aes(ymin=min(df[(1:Npre),yVar]),
    #                                                  ymax=max(df[(Npre+1):nrow(df),yVar])), fill = "grey70", alpha = .50)
  }
  return(plot)

}

