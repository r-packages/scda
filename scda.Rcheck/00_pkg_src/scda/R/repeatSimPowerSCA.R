
#' repeatSimPowerSCA
#' Repeated calls simPowerSCA and stores the results
#'
#'
#' @param nbeep number of assessments per day
#' @param ndays number of days
#' @param effectSizes effect size referring to a bivariate correlation
#' @param iES effect size due to an intervention
#' @param phase0Perc percentage of the observations in first phase
#' @param autocor autocorrelation
#' @param artreat indicates if nothing ("none"), a prewhitening ("prewhitening"), or a lagged predictors ("dynamic") approach is used
#' @param maxiter number of replications
#'
#' @return Mainly, this function computes the power estimates, it returns
#' an object containing two lists: \item{df}{The power estimates} \item{mat}{The raw results.}
#'
#' @author Peter Verboon (Open University of the Netherlands)
#'
#' Maintainer: Gjalt-Jorn Peters <gjalt-jorn@@userfriendlyscience.com>
#' @seealso \code{\link{genlog}}
#'
#'
#' @keywords single case analyses power
#' @rdname repeatSimPowerSCA
#' @examples
#' \dontrun{
#' res <- repeatSimPowerSCA(ndays = 50, iES = 0.50, phase0Perc = .40,
#'         maxiter = 100)
#'}
#' @export repeatSimPowerSCA
#'
repeatSimPowerSCA <- function(nbeep = 1,
                                ndays = NULL,
                                effectSizes = c(0.10, 0.40),
                                iES = 0,
                                phase0Perc = .50,
                                autocor = c(.10, .50),
                                artreat = "none",
                                maxiter = 500){

  if (is.null(ndays)) ndays = seq(20, 140, 10)

  est <- NULL
  out <- array(0, dim = c(length(ndays),length(autocor),length(effectSizes), 11) )

  nextij <- TRUE

  for (j in seq_along(autocor)) {
      for (i in seq_along(ndays)) {
          for (k in seq_along(effectSizes)) {

            if (nextij == FALSE) {
              H0result <- res
            } else {
              H0result <- NULL
            }

      res  <-  simPowerSCA(H0result = H0result,
                           ndays = ndays[i],
                            ES = effectSizes[k],
                            artreat = artreat,
                            iES = iES,
                            phase0Perc = phase0Perc,
                            maxiter= maxiter,
                            ar = autocor[j])
       print(c(i,j,k))
       out[i,j,k,] <-  fill <- apply(res$H1,2,mean)
       est <- rbind(est,fill)

       nextij <- FALSE

          }  ## k
        nextij = TRUE
       }  ## i
   }  ## j


    ndays2 <-   rep(rep(ndays,    each = length(effectSizes)), length(autocor))
    autocorrelation <- rep(autocor, each = length(effectSizes)*length(ndays))
    effectSize <- rep(effectSizes, length(autocor)*length(ndays))
    result <- as.data.frame(cbind(ndays2,autocorrelation,effectSize, est))

    result <- list(df = result, mat = out)

    return(result)

}



