ggGenLogPlot <- function(data,
                         timeVar,
                         yVar,
                         phaseVar = NULL,
                         baselineMeasurements = NULL,
                         base,
                         top,
                         inflectionPoint,
                         yRange = NULL,
                         yFit = NULL,
                         day0 = NULL,
                         day0.formatted =NULL,
                         baseMargin = c(0, 3),
                         topMargin = c(-3, 0),
                         changeDelay = 4,
                         colors = list(bottomBound = viridis::viridis(4)[4],
                                       topBound = viridis::viridis(40)[37],
                                       curve = viridis::viridis(4)[3],
                                       mid = viridis::viridis(4)[2],
                                       intervention = viridis::viridis(4)[1],
                                       points = "black",
                                       outsideRange = "black"),
                         alphas = list(outsideRange = .2,
                                       bounds = .2,
                                       points = .5,
                                       mid = .2),
                         theme = ggplot2::theme_minimal(),
                         pointSize = 2,
                         lineSize = .5,
                         yBreaks = NULL,
                         initialValuesLineType = "dashed",
                         curveSizeMultiplier = 2,
                         plotLabs = NULL,
                         outputFile = NULL,
                         outputWidth = 16,
                         outputHeight = 16,
                         ggsaveParams = list(units='cm',
                                             dpi=300,
                                             type="cairo")) {



  interventionMoment <- mean(data[order(data[, timeVar],
                                        decreasing=FALSE)[c(baselineMeasurements,
                                                            baselineMeasurements+1)], timeVar]);

  if (!is.null(day0) |
      any(class(data[, timeVar]) %in% c('Date', 'POSIXct', 'POSIXt', 'POSIXt'))) {
    data[, timeVar] <-
      as.POSIXct(86400*data[, timeVar],  origin = day0);
      interventionMoment <- as.POSIXct(86400*interventionMoment, origin = day0);
  }

  if (is.null(plotLabs)) {
    plotLabs <- list(x = ifelse(is.null(day0.formatted),
                                "Measurements",
                                "Date"),
                     #paste0("Days since ", day0.formatted)),
                     y = yVar);
  }

  if (is.null(yRange)) {
    yRange <- range(data[, yVar], na.rm=TRUE);
  }

  if (is.null(yBreaks)) {
    yBreaks <- pretty(c(yRange, data[, yVar]),
                      n=(floor(max(yRange) - min(yRange))));
  } else {
    yBreaks <- seq(from = floor(min(yRange)),
                   to = ceiling(max(yRange)),
                   by = yBreaks)
  }

  genlogPlot <-
    ggplot2::ggplot(data, ggplot2::aes_string(x = timeVar, y = yVar)) +

    ### Rectangles showing valid values - we pass one line of
    ### data to make sure the rectangle is only drawn once.
    ggplot2::geom_rect(
      data = data[1,],
      xmin = -Inf,
      xmax = Inf,
      ymin = max(yRange),
      ymax = Inf,
      fill = colors$outsideRange,
      color = NA,
      alpha = alphas$outsideRange
    ) +
    ggplot2::geom_rect(
      data = data[1,],
      xmin = -Inf,
      xmax = Inf,
      ymin = min(yRange),
      ymax = -Inf,
      fill = colors$outsideRange,
      color = NA,
      alpha = alphas$outsideRange
    )


    ### Specified intervention moment
  genlogPlot <-
    genlogPlot + ggplot2::geom_vline(xintercept = interventionMoment,
                        color = colors$intervention,
                        size = lineSize) +
    ggplot2::geom_vline(xintercept = inflectionPoint,
                        colour = colors$mid,
                        size = lineSize) +

    ### Data
    ggplot2::geom_point(size = pointSize,
                        alpha = alphas$points,
                        color = colors$points) +
    theme +
    do.call(ggplot2::labs, plotLabs) +
    ggplot2::coord_cartesian(ylim = yRange) +
    ggplot2::scale_y_continuous(breaks = yBreaks)

  if (!is.null(day0)) {
    genlogPlot <-
      genlogPlot +
      ggplot2::scale_x_datetime(date_breaks="2 months",
                                date_labels="%m-%Y")
  }


    ### Add sigmoid

  genlogPlot <-
    genlogPlot +
      ggplot2::geom_line(
      data = data.frame(x = data[, timeVar], y = yFit),
      ggplot2::aes_string(x = 'x', y = 'y'),
      colour = colors$curve,
      size = lineSize * curveSizeMultiplier
    )


  if (!is.null(outputFile)) {
    ggsaveParameters <- c(list(filename = outputFile,
                               plot = genlogPlot,
                               width = outputWidth,
                               height = outputHeight),
                          ggsaveParams);
    do.call(ggplot2::ggsave, ggsaveParameters);
  }


  return(genlogPlot)

}
