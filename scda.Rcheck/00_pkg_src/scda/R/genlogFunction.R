####  Definition Generalized Logistic function (NB "B" is in exp()) with scaling factor


#' Generalized Logistic Function
#'
#' This function builds a generalized logistic curve from a vector of data points and a set of parameters.
#' The function is used in
#' \code{\link{genlog}}.
#'
#' For details, see Verboon & Peters (2018).
#'
#' @param x A numeric vector with measurement moments or indices of measurement
#' moments.
#' @param x0 A single numeric value specifying at which moment the curve is at
#' its change point from convex to concave: inflection point.
#' @param Ab,At Respectively the lowest (base) and highest (top) possible values of the
#' dependent variable.
#' @param B The growth rate (curve steepness).
#' @author Peter Verboon (Open University of the Netherlands)
#'
#' Maintainer: Gjalt-Jorn Peters <gjalt-jorn@@userfriendlyscience.com>
#' @seealso \code{\link{genlog}}
#' @references Verboon, P. & Peters, G.-J. Y. (2018) Applying the Generalized
#' Logistic Model in Single Case Designs: Modeling Treatment-Induced Shifts.
#' \emph{Behavior Modification}
#' \doi{10.1177/0145445518791255}
#' @keywords utilities
#' @examples
#'
#' time <- 1:20;
#' yVar <- genlogFunction(1:20, 10, 1, 7, 1);
#' plot(time, yVar, type='l', xlab='time', ylab='y');
#'
#' @export genlogFunction
genlogFunction <- function(x, x0, Ab, At, B) {
  return(Ab + ((At - Ab)/ (1 + exp(-B*(x-x0)))));
}
