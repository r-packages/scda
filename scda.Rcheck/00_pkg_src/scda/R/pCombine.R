

 #' pCombine
 #'
 #' This function computes p value from other pvalues using
 #' Fisher's method.
 #' @param pvalues vector of p-values
 #' @export pCombine
 pCombine <- function(pvalues) {
   if (any(pvalues < 0 | pvalues > 1)) {
     stop("Some input values are not p-values")}
   pvalues[pvalues == 0] <- 0.0001
   X <- -2*sum(log(pvalues))
   df <- 2*length(pvalues)
   p <- stats::pchisq(X, df = df, lower.tail = F)
   cat("Chi-square", "df", "   p","\n", round(X,3),"   ",df, round(p,3))
 }
