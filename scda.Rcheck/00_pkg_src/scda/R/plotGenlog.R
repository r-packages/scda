
#' Plots genlog object
#' @param  x  genlog object
#' @param ...  optional parameters for plot
#' @method plot genlog
#' @export
plot.genlog <- function(x, ...) {
  
  data <- x$input$data
  
  genlogPlot <-
  ggGenLogPlot(data = data,
               timeVar = x$intermediate$timeVarName,
               yVar = x$intermediate$yVarName,
               phaseVar = x$intermediate$phaseVarName,
               baselineMeasurements = x$intermediate$baselineMeasurements,
               base = x$output$base,
               top = x$output$top,
               inflectionPoint = x$output$inflectionPoint,
               yRange = x$intermediate$yRange,
               yFit = x$output$yfit,
               day0 = x$intermediate$day0,
               day0.formatted = x$intermediate$day0.formatted,
               changeDelay = x$input$changeDelay,
              ...)
  

  return(genlogPlot)
  
}
