% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/genlog.R
\name{genlog}
\alias{genlog}
\alias{genlogCompleteStartValues}
\alias{ggGenLogPlot}
\title{Generalized Logistic Analysis}
\usage{
genlog(
  data,
  timeVar = 1,
  yVar = 2,
  phaseVar = NULL,
  baselineMeasurements = NULL,
  t_eval = NULL,
  yRange = NULL,
  startInflection = NULL,
  startBase = NULL,
  startTop = NULL,
  startGrowthRate = NULL,
  baseBounds = NULL,
  topBounds = NULL,
  inflectionPointBounds = NULL,
  growthRateBounds = c(-2, 2),
  fixBase = NULL,
  fixTop = NULL,
  fixGrowthRate = NULL,
  fixInflectionPoint = NULL,
  baseMargin = c(0, 3),
  topMargin = c(-3, 0),
  changeDelay = 4,
  maxiter = NULL
)
}
\arguments{
\item{data}{The dataframe containing the variables for the analysis.}

\item{timeVar}{The name of the variable containing the measurement moments
(or an index of measurement moments).}

\item{yVar}{The name of the dependent variable.}

\item{phaseVar}{The variable containing the phase of each measurement. Note
that this normally should only have two possible values.}

\item{baselineMeasurements}{If no phaseVar is specified,
\code{baselineMeasurements} can be used to specify the number of baseline
measurements, which is then used to construct the \code{phaseVar} dummy
variable.}

\item{t_eval}{Point at which effect size (delta) is evaluated. If null then last point is taken.}

\item{yRange}{This can be used to manually specify the possible values that
the dependent variable can take. If no \code{startBase} and \code{startTop}
are specified, the range of the dependent variable is used instead.}

\item{startInflection, startBase, startTop, startGrowthRate}{The
starting values used when estimating the sigmoid using \code{minpack.lm}'s
\code{\link{nlsLM}} function. \code{startX} specifies the starting value to
use for the measurement moment when the change is fastest (i.e. the slope of
the sigmoid has the largest value); \code{startBase} and \code{startTop}
specify the starting values to use for the base (floor) and top (ceiling),
the plateaus of relative stability between which the sigmoid described the
shift; \code{startGrowthRate} specifies the starting value for the growth
rate;}

\item{inflectionPointBounds, growthRateBounds, baseMargin, topMargin, baseBounds, topBounds}{These values specify constraints to respect when estimating the parameters
of the sigmoid function using \code{minpack.lm}'s \code{\link{nlsLM}}.
\code{changeInitiationBounds} specifies between which values the initiation
of the shift must occur; \code{growthRateBounds} describes the bounds
constraining the possible values for the growth rate; \code{baseBounds} and
\code{topBounds} specify the constraints for possible values for the base
(floor) and top (ceiling), the plateaus of relative stability between which
the sigmoid described the shift; and if these are not specified,
\code{baseMargin} and \code{topMargin} are used in combination with the
range of the dependent variable to set these bounds (also see
\code{yRange})}

\item{fixBase}{value for the base which remains fixed and is not estimated}

\item{fixTop}{value for the top which remains fixed and is not estimated}

\item{fixGrowthRate}{value for the growthRate which remains fixed and is not estimated}

\item{fixInflectionPoint}{value for the inflectionPoint which remains fixed and is not estimated}

\item{changeDelay}{The number of measurements to add to the intervention
moment when setting the initial value for the inflection point.}

\item{maxiter}{The maximum number of iterations used by \code{\link{nlsLM}}.}
}
\value{
Mainly, this function prints its results, but it also returns them
in an object containing three lists: \item{input}{The arguments specified
when calling the function} \item{intermediate}{Intermediate objects and
values} \item{output}{The results such as the plot.}
}
\description{
This function implements the generalized logistic analysis introduced in
Verboon & Peters (2020). This analysis fits a logistic function (i.e. a
sigmoid) to a data series. This is useful when analysing single case
designs. The function enables easy customization of the main plot elements
and easy saving of the plot with anti-aliasing. \code{ggGenLogPlot} does
most of the plotting, and can be useful when trying to figure out sensible
starting and boundary/constraint values. \code{genlogCompleteStartValues}
tries to compute sensible starting and boundary/constraint values based on
the data.
}
\details{
For details, see Verboon & Peters (2020).
}
\examples{

### Load dataset
data("Singh", package="scda");

### Extract Jason
dat <- Singh[Singh$tier==1, ];

### Conduct generalized logistic analysis
scda::genlog(
  dat,
  timeVar='time',
  yVar='score_physical',
  phaseVar='phase'
);

}
\references{
Verboon, P. & Peters, G.-J. Y. (2020) Applying the Generalized
Logistic Model in Single Case Designs: Modeling Treatment-Induced Shifts.
\emph{Behavior Modification, 44(1)}
\doi{10.1177/0145445518791255}
}
\author{
Peter Verboon & Gjalt-Jorn Peters (both at the Open University of
the Netherlands)

Maintainer: Gjalt-Jorn Peters \href{mailto:gjalt-jorn@userfriendlyscience.com}{gjalt-jorn@userfriendlyscience.com}
}
\keyword{hplot}
\keyword{htest}
\keyword{models}
