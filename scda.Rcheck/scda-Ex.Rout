
R version 4.1.0 (2021-05-18) -- "Camp Pontanezen"
Copyright (C) 2021 The R Foundation for Statistical Computing
Platform: x86_64-apple-darwin17.0 (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> pkgname <- "scda"
> source(file.path(R.home("share"), "R", "examples-header.R"))
> options(warn = 1)
> base::assign(".ExTimings", "scda-Ex.timings", pos = 'CheckExEnv')
> base::cat("name\tuser\tsystem\telapsed\n", file=base::get(".ExTimings", pos = 'CheckExEnv'))
> base::assign(".format_ptime",
+ function(x) {
+   if(!is.na(x[4L])) x[1L] <- x[1L] + x[4L]
+   if(!is.na(x[5L])) x[2L] <- x[2L] + x[5L]
+   options(OutDec = '.')
+   format(x[1L:3L], digits = 7L)
+ },
+ pos = 'CheckExEnv')
> 
> ### * </HEADER>
> library('scda')
> 
> base::assign(".oldSearch", base::search(), pos = 'CheckExEnv')
> base::assign(".old_wd", base::getwd(), pos = 'CheckExEnv')
> cleanEx()
> nameEx("Franco")
> ### * Franco
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: Franco
> ### Title: Interaction scores in multiple baseline AB design from 6
> ###   children, taken from Franco et al. (2013)
> ### Aliases: Franco
> ### Keywords: datasets
> 
> ### ** Examples
> 
> 
> ### To load the data, use:
> data(Franco);
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("Franco", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("IRD")
> ### * IRD
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: IRD
> ### Title: Robust improvement rate difference
> ### Aliases: IRD
> 
> ### ** Examples
> 
> A <- c(20, 20, 26, 25, 22, 23)
> B <- c(28, 25, 24, 27, 30, 30, 29)
> AB <- c(A,B)
> IRD(score = AB, n_a = 6)
[1] 0.6904762
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("IRD", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("NAP")
> ### * NAP
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: NAP
> ### Title: Non-overlap of all pairs
> ### Aliases: NAP
> 
> ### ** Examples
> 
> A <- c(20, 20, 26, 25, 22, 23)
> B <- c(28, 25, 24, 27, 30, 30, 29)
> AB <- c(A,B)
> NAP(score = AB, n_a = 6)
$Est
[1] 0.9166667

$SE
[1] 0.07739185

$CI
    lower     upper 
0.5973406 0.9860176 

> 
> # Example from Parker & Vannest (2009)
> yA <- c(4, 3, 4, 3, 4, 7, 5, 2, 3, 2)
> yB <- c(5, 9, 7, 9, 7, 5, 9, 11, 11, 10, 9)
> yAB <- c(yA,yB)
> NAP(score = yAB, n_a = 6)
$Est
[1] 0.7666667

$SE
[1] 0.1050279

$CI
    lower     upper 
0.4870424 0.9128502 

> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("NAP", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("PAND")
> ### * PAND
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: PAND
> ### Title: Percentage of all non-overlapping data (PAND)
> ### Aliases: PAND
> 
> ### ** Examples
> 
> A <- c(20, 20, 26, 25, 22, 23)
> B <- c(28, 25, 24, 27, 30, 30, 29)
> AB <- c(A,B)
> PAND(score = AB, n_a = 6)
[1] 0.8461538
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("PAND", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("PEM")
> ### * PEM
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: PEM
> ### Title: Percentage exceeding the median
> ### Aliases: PEM
> 
> ### ** Examples
> 
> A <- c(20, 20, 26, 25, 22, 23)
> B <- c(28, 25, 24, 27, 30, 30, 29)
> AB <- c(A,B)
> PEM(score = AB, n_a = 6)
[1] 1
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("PEM", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("PND")
> ### * PND
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: PND
> ### Title: Percentage of non-overlapping data
> ### Aliases: PND
> 
> ### ** Examples
> 
> A <- c(20, 20, 26, 25, 22, 23)
> B <- c(28, 25, 24, 27, 30, 30, 29)
> AB <- c(A,B)
> PND(score = AB, n_a = 6)
[1] 0.7142857
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("PND", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("Singh")
> ### * Singh
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: Singh
> ### Title: Verbal and physical aggression scores from Singh et al. (2007)
> ### Aliases: Singh
> ### Keywords: datasets
> 
> ### ** Examples
> 
> 
> ### To load the data, use:
> data(Singh);
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("Singh", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("Tau")
> ### * Tau
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: Tau
> ### Title: Tau (non-overlap)
> ### Aliases: Tau
> 
> ### ** Examples
> 
> A <- c(20, 20, 26, 25, 22, 23)
> B <- c(28, 25, 24, 27, 30, 30, 29)
> AB <- c(A,B)
> Tau(score = AB, n_a = 6)
$Est
[1] 0.8333333

$SE
[1] 0.1547837

$CI
    lower     upper 
0.1946812 0.9720352 

> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("Tau", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("Tau_U")
> ### * Tau_U
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: Tau_U
> ### Title: Tau-U
> ### Aliases: Tau_U
> 
> ### ** Examples
> 
> A <- c(20, 20, 26, 25, 22, 23)
> B <- c(28, 25, 24, 27, 30, 30, 29)
> AB <- c(A,B)
> Tau_U(score = AB, n_a = 6)
[1] 0.7380952
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("Tau_U", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("cyclic")
> ### * cyclic
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: cyclic
> ### Title: Cyclic Analysis
> ### Aliases: cyclic
> ### Keywords: cyclic sinus
> 
> ### ** Examples
> 
> 
> ### Load dataset
> data("Singh", package="scda");
> 
> ### Extract Jason
> dat <- Singh[Singh$tier==1, ];
> 
> ### Conduct cyclic analysis
> scda::cyclic(
+   dat,
+   timeVar='time',
+   yVar='score_physical',
+   phaseVar='phase'
+ );
Warning in scda::cyclic(dat, timeVar = "time", yVar = "score_physical",  :
  The fit of the cyclic function is *worse* than simply estimating the linear fit as best prediction for all observations. 
This suggests that the  model may not be appropriate for these data, or that the initial or boundary values should be adjusted. 
Inspect the data closely.

R squared from cyclic model: 0.632
R squared from linear model: 0.663 

Effect size cyclic model: -0.092 

Estimated Period: 32 

> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("cyclic", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("cyclicML")
> ### * cyclicML
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: cyclicML
> ### Title: Cyclic Multilevel Analysis
> ### Aliases: cyclicML
> ### Keywords: case curve logistic models single
> 
> ### ** Examples
> 
> 
> ### Load dataset
> data(Singh);
> ## Conduct cyclic multilevel analysis
> scda::cyclicML(
+   Singh,
+   timeVar='time',
+   yVar='score_verbal',
+   groupVar = 'id',
+   fixSlope = 0)
Fitted Model: 
score_verbal ~ b0 + b1 * cos(w * (time - b2)) + 0 * time

Deviance:  250.253
Degrees of freedom: 6
R squared from mixed cyclic model: 0.538
R squared from mixed linear model: 0.487 
Effect size cyclic model: 0.099 


Parameter estimates: 
Approximate 95% confidence intervals

 Fixed effects:
     lower    est. upper
b0 -13.909   6.656 27.22
b1 -14.838   5.294 25.43
b2 -84.852 -12.568 59.72
w   -0.386  -0.108  0.17
attr(,"label")
[1] "Fixed effects:"
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("cyclicML", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("genPwr")
> ### * genPwr
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: genPwr
> ### Title: Generalized piecewise regression analysis
> ### Aliases: genPwr print.genPwr plot.genPwr
> ### Keywords: piecewise regression reversal withdrawal
> 
> ### ** Examples
> 
> 
> time <- c(0:29)
> score <- c(4,2,3,4,3,4,3,5,6,7,6,7,8,8,7,5,6,4,5,5,6,5,5,4,4,5,6,7,6,7)
> fase4 <- as.factor(c(rep("a",7), rep("b",8), rep("c",7), rep("d",8)))
> dat <- data.frame(time = time, score = score, fase4 = fase4)
> result <- genPwr(data = dat, yVar = "score", phaseVar = "fase4", timeVar = "time")
> plot(result)
> print(result)
Generalized Piecewise Regression (N = 30)

Model statistics:

  Model deviance:              12.2
  R squared for null model:    .169
  R squared for test model:    .986
  R squared based effect size: .984
  Standardized effect size:    3.915

Regression coefficients
-----------------------------------------------
effect     estimate     low_lim CI   upp_lim CI
---------- ---------- ------------ ------------
level A1   3.393             2.342        4.444

level B1   7.917             6.921        8.912

level A2   5.25              4.199        6.301

level B2   6.917             5.921        7.912

trend A1   0.036            -0.256        0.327

trend B1   0.333             0.095        0.571

trend A2   0.036            -0.256        0.327

trend B2   0.405             0.167        0.643
-----------------------------------------------

> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("genPwr", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("genlog")
> ### * genlog
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: genlog
> ### Title: Generalized Logistic Analysis
> ### Aliases: genlog genlogCompleteStartValues ggGenLogPlot
> ### Keywords: hplot htest models
> 
> ### ** Examples
> 
> 
> ### Load dataset
> data("Singh", package="scda");
> 
> ### Extract Jason
> dat <- Singh[Singh$tier==1, ];
> 
> ### Conduct generalized logistic analysis
> scda::genlog(
+   dat,
+   timeVar='time',
+   yVar='score_physical',
+   phaseVar='phase'
+ );
Generalized Logistic Analysis (N = 16)

Estimated sigmoid association between time and score_physical.

Pre-specified (non-estimated) parameters: 

Estimated and pre-specified parameters:

  Curve base (plateau after change):  0.031
  Curve top (plateau before change):  4
  Growth rate:                        -1.24
  Inflection point:                   4.36

Model fit and effect sizes estimates:

  Deviance:              2.37
  R squared:             0.923
  ESc (Cohen's d-based): 2.76
  ESr (range-based):     0.992
  ESt (delta_ts):        2.21


-----------------------------------------
Start values used in optimization
           startBase             startTop      startGrowthRate 
                   0                    4                    0 
startInflectionPoint 
                   7 

Boundary values used in optimization
 baseBounds topBounds growthRateBounds inflectionPointBounds
          0         1               -2                     2
          3         4                2                    12
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("genlog", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("genlogFunction")
> ### * genlogFunction
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: genlogFunction
> ### Title: Generalized Logistic Function
> ### Aliases: genlogFunction
> ### Keywords: utilities
> 
> ### ** Examples
> 
> 
> time <- 1:20;
> yVar <- genlogFunction(1:20, 10, 1, 7, 1);
> plot(time, yVar, type='l', xlab='time', ylab='y');
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("genlogFunction", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("genlogML")
> ### * genlogML
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: genlogML
> ### Title: Generalized Logistic Multilevel Analysis
> ### Aliases: genlogML
> ### Keywords: case curve logistic models single
> 
> ### ** Examples
> 
> 
> ### Load dataset
> data(Singh);
> 
> ### Conduct generalized logistic multilevel analysis
> scda::genlogML(
+   Singh,
+   timeVar='time',
+   yVar='score_verbal',
+   groupVar = 'tier',
+   randomPar = 'ip',
+   startGrowthRate = -2
+ );
Fitted Model: score_verbal ~ 0 + 14/(1 + exp(-growthRate * (time - inflectionPoint)))

Summary of analysis                   

Nonlinear mixed-effects model fit by maximum likelihood
  Model: ft 
  Data: data 
  AIC BIC logLik
  258 266   -125

Random effects:
 Formula: inflectionPoint ~ 1 | tier
        inflectionPoint Residual
StdDev:            3.06     2.13

Fixed effects:  list(ff) 
                 Value Std.Error DF t-value p-value
growthRate      -0.177     0.031 52   -5.65   0.000
inflectionPoint  1.507     2.081 52    0.72   0.472
 Correlation: 
                grwthR
inflectionPoint -0.357

Standardized Within-Group Residuals:
   Min     Q1    Med     Q3    Max 
-2.735 -0.521 -0.122  0.672  2.856 

Number of Observations: 56
Number of Groups: 3 
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("genlogML", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("lagESM")
> ### * lagESM
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: lagESM
> ### Title: Function to compute lagged n variables in ESM data and add it to
> ###   data
> ### Aliases: lagESM
> 
> ### ** Examples
> 
> data("news")
Warning in data("news") : data set ‘news’ not found
> vars <- c("Fearful","Hopeful","Anxious","Down","Inspiring","Irritated","Relaxed","Insecure")
> res <- lagESM(data = news, subjnr="subjnr", level2= "daynr",
+        level1 = "beepnr", lagn = 1, varnames = vars)
Name of subjnr is not correctly specified in function lagESM 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("lagESM", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("nonOverlap")
> ### * nonOverlap
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: nonOverlap
> ### Title: nonOverlap
> ### Aliases: nonOverlap
> ### Keywords: analyses case single
> 
> ### ** Examples
> 
> ### Get example data and extract data for Jason
> data("Singh", package="scda");
> dat <- Singh[Singh$tier==1, ];
> 
> ### Compute non-overlap
> scda::nonOverlap(
+   dat,
+   timeVar='time',
+   yVar='score_verbal',
+   phaseVar = NULL,
+   baselineMeasurements=5
+ );
   name statistic
1   PND     0.000
2  PAND     0.688
3   PEM     0.000
4   NAP     0.027
5   IRD     0.273
6   TAU    -0.945
7 Tau_U    -0.836
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("nonOverlap", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("piecewiseRegr")
> ### * piecewiseRegr
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: piecewiseRegr
> ### Title: Piecewise regression analysis
> ### Aliases: piecewiseRegr print.piecewiseRegr
> ### Keywords: hplot htest regression
> 
> ### ** Examples
> 
> 
> ### Load dataset
> data(Singh);
> 
> ### Extract Jason
> dat <- Singh[Singh$tier==1, ];
> 
> ### Conduct piecewise regression analysis
> piecewiseRegr(dat,
+               timeVar='time',
+               yVar='score_physical',
+               phaseVar='phase');
Piecewise Regression Analysis (N = 16)

Model statistics:

  Model deviance:                         6.03
  R squared for null model:               0.66
  R squared for test model:               0.81
  R squared based effect size:            0.42
  Effect size (delta_t):                  -3.95
  Standardized effect size (delta_ts):    -6.23
  Effect size (delta):                    -2.95
  Standardized effect size (delta_s):     -4.65

  Effect size evaluated at point:         16

Regression coefficients:

  Intercept:        [1.92; 4.74] (point estimate = 3.33)
  Level change:     [-4.59; 0.4] (point estimate = -2.09)
  Trend phase 1:   [-1.09; 1.09] (point estimate = 0)
  Change in trend: [-1.24; 0.96] (point estimate = -0.14)> 
> ### Pretend treatment started between measurements
> ### 5 and 6
> piecewiseRegr(dat,
+               timeVar='time',
+               yVar='score_physical',
+               baselineMeasurements=5);
Piecewise Regression Analysis (N = 16)

Model statistics:

  Model deviance:                         3.06
  R squared for null model:               0.66
  R squared for test model:               0.9
  R squared based effect size:            0.71
  Effect size (delta_t):                  6.57
  Standardized effect size (delta_ts):    14.5
  Effect size (delta):                    1.29
  Standardized effect size (delta_s):     2.86

  Effect size evaluated at point:         16

Regression coefficients:

  Intercept:         [2.95; 4.65] (point estimate = 3.8)
  Level change:     [-2.34; 0.28] (point estimate = -1.03)
  Trend phase 1:   [-0.85; -0.15] (point estimate = -0.5)
  Change in trend:    [0.1; 0.83] (point estimate = 0.46)> 
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("piecewiseRegr", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("pwMLRegr")
> ### * pwMLRegr
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: pwMLRegr
> ### Title: Piecewise multilevel regression analysis
> ### Aliases: pwMLRegr
> ### Keywords: multilevel piecewise regression
> 
> ### ** Examples
> 
> 
> ### Conduct piecewise multilevel regression analysis
> data("Franco")
> result <- pwMLRegr(data = Franco,
+                  yVar = "score",
+                 phaseVar = "phase",
+                 subjectID = "id")
> 
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("pwMLRegr", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("randomTestMLShift")
> ### * randomTestMLShift
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: randomTestMLShift
> ### Title: randomTestMLShift
> ### Aliases: randomTestMLShift
> ### Keywords: multilevel randomisation_test
> 
> ### ** Examples
> 
> ## Not run: 
> ##D data(Franco)
> ##D result <- randomTestMLShift(dat = Franco, yVar = "score", phaseVar = "phase",
> ##D subjectID = "id", minPhase = 3)
> ## End(Not run)
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("randomTestMLShift", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("randomTestNonOverlap")
> ### * randomTestNonOverlap
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: randomTestNonOverlap
> ### Title: randomTestNonOverlap
> ### Aliases: randomTestNonOverlap
> ### Keywords: permation_test single_case_analyses
> 
> ### ** Examples
> 
> dat <- Singh[Singh$tier==1, ];
> randomTestNonOverlap(data = dat, yVar='score_verbal', baselineMeasurements = 5, minPhase = 5,
+ improvement = "decrease")
$input
$input$data
   tier    id time phase score_physical score_verbal
1     1 Jason    1     0              3           12
2     1 Jason    2     0              4           10
3     1 Jason    3     0              3           14
4     1 Jason    4     1              3            8
5     1 Jason    5     1              1            5
6     1 Jason    6     1              0            5
7     1 Jason    7     1              1            4
8     1 Jason    8     1              0            7
9     1 Jason    9     1              0            4
10    1 Jason   10     1              0            3
11    1 Jason   11     1              0            3
12    1 Jason   12     1              0            0
13    1 Jason   13     1              0            0
14    1 Jason   14     1              0            2
15    1 Jason   15     1              0            0
16    1 Jason   16     1              0            0

$input$yVar
[1] "score_verbal"

$input$phaseVar
NULL

$input$baselineMeasurements
[1] 5

$input$minPhase
[1] 5

$input$improvement
[1] "decrease"


$intermediate
list()

$output
$output$observed
  PND  PAND   PEM   NAP   IRD   TAU Tau_U 
0.818 0.938 1.000 0.973 0.855 0.945 0.836 

$output$pvalues
  PND  PAND   PEM   NAP   IRD   TAU Tau_U 
 0.86  0.86  1.00  0.71  0.86  0.71  0.14 

$output$permutations
    PND  PAND PEM   NAP   IRD   TAU Tau_U
1 0.818 0.938   1 0.973 0.855 0.945 0.836
2 0.900 0.938   1 0.967 0.867 0.933 0.767
3 0.778 0.875   1 0.944 0.746 0.889 0.635
4 0.875 0.938   1 0.992 0.875 0.984 0.719
5 1.000 1.000   1 1.000 1.000 1.000 0.619
6 0.833 0.938   1 0.992 0.867 0.983 0.433
7 1.000 1.000   1 1.000 1.000 1.000 0.236

$output$result
    PND  PAND  PEM   NAP   IRD   TAU Tau_U
1  TRUE  TRUE TRUE  TRUE  TRUE  TRUE  TRUE
2  TRUE  TRUE TRUE FALSE  TRUE FALSE FALSE
3 FALSE FALSE TRUE FALSE FALSE FALSE FALSE
4  TRUE  TRUE TRUE  TRUE  TRUE  TRUE FALSE
5  TRUE  TRUE TRUE  TRUE  TRUE  TRUE FALSE
6  TRUE  TRUE TRUE  TRUE  TRUE  TRUE FALSE
7  TRUE  TRUE TRUE  TRUE  TRUE  TRUE FALSE


> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("randomTestNonOverlap", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("repeatSimPowerSCA")
> ### * repeatSimPowerSCA
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: repeatSimPowerSCA
> ### Title: repeatSimPowerSCA Repeated calls simPowerSCA and stores the
> ###   results
> ### Aliases: repeatSimPowerSCA
> ### Keywords: analyses case power single
> 
> ### ** Examples
> 
> ## Not run: 
> ##D res <- repeatSimPowerSCA(ndays = 50, iES = 0.50, phase0Perc = .40,
> ##D         maxiter = 100)
> ## End(Not run)
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("repeatSimPowerSCA", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("simPowerML")
> ### * simPowerML
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: simPowerML
> ### Title: simPowerML
> ### Aliases: simPowerML
> ### Keywords: analyses case power single
> 
> ### ** Examples
> 
> ## Not run: 
> ##D res <- simPowerML(nsubj = 12, ndays = 25, maxiter = 1000, ES1 = 1, ES2 = 1)
> ## End(Not run)
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("simPowerML", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("simPowerPWR")
> ### * simPowerPWR
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: simPowerPWR
> ### Title: simPowerPWR
> ### Aliases: simPowerPWR
> ### Keywords: analyses case power single
> 
> ### ** Examples
> 
> ## Not run: 
> ##D res <- simPowerPWR(ndays = 25, maxiter = 100, ES1 = 1, ES2 = 2)
> ## End(Not run)
> 
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("simPowerPWR", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> cleanEx()
> nameEx("simPowerSCA")
> ### * simPowerSCA
> 
> flush(stderr()); flush(stdout())
> 
> base::assign(".ptime", proc.time(), pos = "CheckExEnv")
> ### Name: simPowerSCA
> ### Title: simPowerSCA
> ### Aliases: simPowerSCA
> ### Keywords: analyses case power single
> 
> ### ** Examples
> 
> ## Not run: 
> ##D res <- simPowerSCA(ndays = 50, ES = 0.40, iES = 0.50, phase0Perc = .40,
> ##D ar = 0.2, maxiter = 1000)
> ## End(Not run)
> 
> 
> 
> base::assign(".dptime", (proc.time() - get(".ptime", pos = "CheckExEnv")), pos = "CheckExEnv")
> base::cat("simPowerSCA", base::get(".format_ptime", pos = 'CheckExEnv')(get(".dptime", pos = "CheckExEnv")), "\n", file=base::get(".ExTimings", pos = 'CheckExEnv'), append=TRUE, sep="\t")
> ### * <FOOTER>
> ###
> cleanEx()
> options(digits = 7L)
> base::cat("Time elapsed: ", proc.time() - base::get("ptime", pos = 'CheckExEnv'),"\n")
Time elapsed:  3.58 0.166 3.818 0.003 0.003 
> grDevices::dev.off()
null device 
          1 
> ###
> ### Local variables: ***
> ### mode: outline-minor ***
> ### outline-regexp: "\\(> \\)?### [*]+" ***
> ### End: ***
> quit('no')
