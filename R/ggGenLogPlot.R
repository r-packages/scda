ggGenLogPlot <- function(data,
                         timeVar,
                         yVar,
                         phaseVar = NULL,
                         baselineMeasurements = NULL,
                         base,
                         top,
                         inflectionPoint,
                         yRange = NULL,
                         yFit = NULL,
                         day0 = NULL,
                         day0.formatted =NULL,
                         baseMargin = c(0, 3),
                         topMargin = c(-3, 0),
                         changeDelay = 4,
                         colors = list(bottomBound = viridis::viridis(4)[4],
                                       topBound = viridis::viridis(40)[37],
                                       curve = viridis::viridis(4)[3],
                                       mid = viridis::viridis(4)[2],
                                       intervention = viridis::viridis(4)[1],
                                       points = "black",
                                       outsideRange = "black"),
                         alphas = list(outsideRange = .2,
                                       bounds = .2,
                                       points = .5,
                                       mid = .2),
                         theme = ggplot2::theme_minimal(),
                         pointSize = 2,
                         linewidth = .5,
                         initialValuesLineType = "dashed",
                         curveSizeMultiplier = 2,
                         plotLabs = NULL,
                         outputFile = NULL,
                         outputWidth = 16,
                         outputHeight = 16,
                         ggsaveParams = list(units='cm',
                                             dpi=300,
                                             type="cairo")) {



  interventionMoment <- mean(data[order(data[, timeVar],
                                        decreasing=FALSE)[c(baselineMeasurements,
                                                            baselineMeasurements+1)], timeVar]);

  if (!is.null(day0) |
      any(class(data[, timeVar]) %in% c('Date', 'POSIXct', 'POSIXt', 'POSIXt'))) {
    data[, timeVar] <-
      as.POSIXct(86400*data[, timeVar],  origin = day0);
      interventionMoment <- as.POSIXct(86400*interventionMoment, origin = day0);
  }

  if (is.null(plotLabs)) {
    plotLabs <- list(x = ifelse(is.null(day0.formatted),
                                "Measurements",
                                "Date"),
                     #paste0("Days since ", day0.formatted)),
                     y = yVar);
  }

  if (is.null(yRange)) {
    yRange <- range(data[, yVar], na.rm=TRUE);
  }

  df0 <- data.frame(x=data[,timeVar], y = data[,yVar])

  genlogPlot <-
    ggplot2::ggplot(df0, ggplot2::aes(x=x, y = y))


    ### Specified intervention moment
  genlogPlot <-
    genlogPlot + ggplot2::geom_vline(xintercept = interventionMoment,
                        color = colors$intervention,
                        linewidth = linewidth) +
    ggplot2::geom_vline(xintercept = inflectionPoint,
                        colour = colors$mid,
                        linewidth = linewidth) +

    ### Data
    ggplot2::geom_point(size = pointSize,
                        alpha = alphas$points,
                        color = colors$points) +
    theme +
    do.call(ggplot2::labs, plotLabs) +
    ggplot2::coord_cartesian(ylim = yRange)

  if (!is.null(day0)) {
    genlogPlot <-
      genlogPlot +
      ggplot2::scale_x_datetime(date_breaks="2 months",
                                date_labels="%m-%Y")
  }


    ### Add sigmoid

  df <- data.frame(x = data[, timeVar], y = yFit)

  genlogPlot <-
    genlogPlot +
      ggplot2::geom_line(
        data =df,
        ggplot2::aes(x = x, y = y),
        colour = colors$curve,
      linewidth = linewidth * curveSizeMultiplier
    )


  if (!is.null(outputFile)) {
    ggsaveParameters <- c(list(filename = outputFile,
                               plot = genlogPlot,
                               width = outputWidth,
                               height = outputHeight),
                          ggsaveParams);
    do.call(ggplot2::ggsave, ggsaveParameters);
  }


  return(genlogPlot)

}
