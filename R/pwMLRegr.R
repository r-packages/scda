#' Piecewise multilevel regression analysis
#'
#' This function conducts a piecewise multilevel regression analysis and shows a plot
#' illustrating the results. The function enables easy customization of the
#' main plot elements and easy saving of the plot with anti-aliasing.
#'
#'
#' @param data The dataframe containing the variables for the analysis.
#' @param timeVar The name of the variable containing the measurement moments
#' (or an index of measurement moments). An index can also be specified, and
#' assumed to be 1 if omitted.
#' @param yVar The name of the dependent variable. An index can also be
#' specified, and assumed to be 2 if omitted.
#' @param phaseVar The variable containing the phase of each measurement. Note
#' that this normally should only have two possible values.
#' @param subjectID variabele indicating the group level (usually the subjects)
#' @param evalTime Point at which effect is evaluated. If empty last point is taken.
#' @param alpha Alpha used to construct confidence interval.
#' @return Mainly, this function prints the results of four models,
#' including two piecewise regression models. Effect sizes with confidence intervals are given.
#'
#' @author Peter Verboon & Gjalt-Jorn Peters (both at the Open University of
#' the Netherlands)
#'
#' Maintainer: Gjalt-Jorn Peters <gjalt-jorn@@userfriendlyscience.com>
#' @seealso \code{\link{genlog}}
#'
#' @references Pustejovsky, J. E.Hedges, L. V, Shadish, W. R. (2014).Design-Comparable Effect Sizes in
#' Multiple Baseline Designs: A General Modeling Framework.
#' \emph{Journal of Educational and Behavioral Statistics}
#' \doi{10.3102/1076998614547577}
#'
#' @keywords piecewise multilevel regression
#' @rdname pwMLRegr
#' @examples
#'
#' ### Conduct piecewise multilevel regression analysis
#' data("Franco")
#' result <- pwMLRegr(data = Franco,
#'                  yVar = "score",
#'                 phaseVar = "phase",
#'                 subjectID = "id")
#'
#'
#' @export pwMLRegr
pwMLRegr <- function(data,
                     yVar,
                     phaseVar,
                     timeVar = NULL,
                     subjectID,
                     evalTime = NULL,
                     alpha = 0.05
) {

  res <- list(input = as.list(environment()),
              intermediate = list(),
              output = list());

  if (is.null(timeVar)) {
    timeVar <- "time"
    data$time <- sequence(rle(data[,subjectID])$lengths)
  }

  if (is.numeric(data[, phaseVar])) data[, phaseVar] <- as.factor(data[, phaseVar])
  if (is.character(data[, phaseVar])) data[, phaseVar] <- as.factor(data[, phaseVar])

  subjectNumbers <- unique(data[,subjectID])
  data$trendTerm <- 0
  subjectnA <- rep(0, length(subjectNumbers))

  for (i in seq_along(subjectNumbers)) {

    s <-  data[,subjectID] == subjectNumbers[i]
    ### Get minimum and maximum of phase variable
    phaseVarMin <- min(levels(data[s, phaseVar]));
    phaseVarMax <- max(levels(data[s, phaseVar]));

    ### If the time variable is actually provided as time instead of as
    ### indices/ranks, convert to numeric first.
    if (!is.numeric(data[, timeVar])) {
      if (any(class(data[, timeVar]) %in% c('Date', 'POSIXct', 'POSIXt', 'POSIXt'))) {
        res$intermediate$day0 <- min(data[, timeVar]);
        res$intermediate$day0.formatted <- as.character(res$intermediate$day0);
        data[, timeVar] <- as.numeric(data[, timeVar]) - as.numeric(min(data[, timeVar]));
      } else {
        stop("The timeVar variable does not have a class I can work with (numeric or date): instead it has class ",
             ufs::vecTxtQ(class(data[, timeVar])), ".");
      }
    }
    ### Tc is adjusted to start with 0
    data[s, timeVar] <- data[s, timeVar] - min(data[s, timeVar]);

    subjectnA[i] <- nA <- sum(data[s, phaseVar] == phaseVarMin);

    ### Trend term for phase B  (see Huitema & Kean, 2000)
    data[s,"trendTerm"] <- ifelse(data[s, phaseVar] == phaseVarMax,
                                  data[s, timeVar] - data[nA + 1, timeVar],
                                  0);
  }

  ## if evaluation time is not specified, take the minimum last time points of all subjects
  if (is.null(evalTime)) evalTime <- min(tapply(data[,timeVar],data[,subjectID], max) - subjectnA)
  res$intermediate$evalTime <- evalTime



  df <- data.frame(subjectID = data[,subjectID],
                   yVar = data[,yVar],
                   phaseVar = data[,phaseVar],
                   timeVar = data[,timeVar],
                   trendTerm = data[,"trendTerm"])


  ### Fit piecewise ML models

  g1 <- g2 <- g3 <- g4 <- NULL
  ES2 <- ES3 <- ES4 <- NA

  ## Model 1  # fixed: fase , random: intercept only, no auto-correlation
  res$intermediate$lm.model1 <- model1 <-
    nlme::lme(fixed = yVar ~ phaseVar,
              random = ~ 1  | subjectID,
              data = df, na.action = stats::na.omit )

  ES0 <- nlme::fixef(model1)[2]/sqrt(sum(as.numeric(nlme::VarCorr(model1)[,1])))
  icc1 <- multilevel::ICC1(stats::aov(yVar ~ as.factor(subjectID), data = df))
  if (icc1 < 0) icc1 <- 0


  ## Model 2  # fixed: fase , random: intercept only, with auto-correlation
  res$intermediate$lm.model2 <- model2 <-
    nlme::lme(fixed = yVar ~ phaseVar,
              random = ~ 1  | subjectID,
              correlation = nlme::corAR1(0, ~ timeVar | subjectID),
              data = df, na.action = stats::na.omit )

  ES1 <- nlme::fixef(model2)[2]/sqrt(sum(as.numeric(nlme::VarCorr(model2)[,1])))
  res$intermediate$g1 <- g1 <-
    try(scdhlm::g_mlm(model2, p_const = c(0,1), r_const = c(1,0,1)), silent = TRUE)
    if (inherits(g1, "try-error")) g1 <- NULL

  
  ## Model 3 (new)  # fixed: piecewise regression model without AR , random: intercept only
  res$intermediate$lm.model3 <- 
  model3 <-
    try(nlme::lme(fixed = yVar ~ phaseVar + timeVar + trendTerm,
                  random =  ~ 1  |  subjectID,
                  data=df, na.action = stats::na.omit),
        silent = TRUE)
  
  if (!inherits(model3, "try-error")) {
    BminA <- evalTime
    v <- c(1,BminA)
    ES2 <- ((nlme::fixef(model3)[c(2,4)])%*%v)/sqrt(sum(as.numeric(nlme::VarCorr(model3)[,1])))
    res$intermediate$g2 <- 
    g2 <-
      try(scdhlm::g_mlm(model3, p_const = c(0,1,0,1), r_const = c(1,1)), silent = TRUE)
     if (inherits(g2, "try-error")) g2 <- NULL
  }
  
  

  ## Model 4  # fixed: piecewise regression model , random: intercept only
  res$intermediate$lm.model4 <- model4 <-
    try(nlme::lme(fixed = yVar ~ phaseVar + timeVar + trendTerm,
                  random =  ~ 1  |  subjectID,
                  correlation = nlme::corAR1(0, ~  timeVar | subjectID),
                  data=df, na.action = stats::na.omit),
        silent = TRUE)

  if (!inherits(model4, "try-error")) {
    BminA <- evalTime
    v <- c(1,BminA)
    ES3 <- ((nlme::fixef(model4)[c(2,4)])%*%v)/sqrt(sum(as.numeric(nlme::VarCorr(model4)[,1])))
    res$intermediate$g3 <- g3 <-
      try(scdhlm::g_mlm(model4, p_const = c(0,1,0,1), r_const = c(1,0,1)), silent = TRUE)
      if (inherits(g3, "try-error")) g3 <- NULL
  }


  ## Model 5  # fixed: piecewise regression model, random: intercept and timeVar
  res$intermediate$lm.model5 <- model5 <-
    try(nlme::lme(fixed = yVar ~ phaseVar + timeVar + trendTerm,
                  random =  ~ 1 +  timeVar |  subjectID,
                  correlation = nlme::corAR1(0, ~  timeVar | subjectID),
                  data=df, na.action = stats::na.omit),
        silent = TRUE)

  if (!inherits(model5, "try-error")) {
    BminA <- evalTime
    v <- c(1,BminA)
    BminC <- evalTime
    w <- c(2*BminC, BminC^2)
    terms <- as.numeric(c(nlme::getVarCov(model5)[2,1], nlme::VarCorr(model5)[2,1]))   ## tau20, tau2sq
    D1 <- sum(as.numeric(nlme::VarCorr(model5)[c(1,3),1]))
    D2 <- terms %*% w
    ES3 <- ((nlme::fixef(model5)[c(2,4)])%*%v)/sqrt(D1 + D2)
    res$intermediate$g4 <- g4 <-
      try(scdhlm::g_mlm(model5, p_const = c(0,1,0,1), r_const = c(1,0,1,w[1],w[2])), silent = TRUE)
      if (inherits(g4, "try-error")) g4 <- NULL
  }


  ## Confidence interval following Pustejovsky et al. (2014, formula 16)
  CI0 <- confIntES(model = model1, p=c(0,1), r=c(1,1), alpha = alpha)$CI            #  df = 4
  CI1 <- confIntES(model = model2, p=c(0,1), r=c(1,0,1), alpha = alpha)$CI          #  df = 5

  tlimit2 <- stats::qt(alpha/2, 7, lower.tail = FALSE)
  tlimit3 <- stats::qt(alpha/2, 9, lower.tail = FALSE)

  ESg1 <- ESg2 <- ESg3 <- ESg4 <- NA
  CI2 <- CI3 <- CI4 <- NA
  ## take care of extreme small variances
  if (!is.null(g1)) {
    if (g1$nu > 1) { ESg1 <- g1$g_AB
  } else {
    ESg1 <- g1$delta_AB }
  }

  if (!is.null(g2)) {
    if (g2$nu > 1) { ESg2 <- g2$g_AB
    } else {
      ESg2 <- g2$delta_AB }
      CI2 <- scdhlm::CI_g(g2, symmetric = TRUE, cover = 0.95)   
  }
  
  if (!is.null(g3)) {
    if (g3$nu > 1) { ESg3 <- g3$g_AB
    } else {
      ESg3 <- g3$delta_AB }
      CI3 <- c(ESg3 - g3$SE_g_AB*tlimit2 ,  ESg3 + g3$SE_g_AB*tlimit2)   # alpa = .05, df = 7
  }

  if (!is.null(g4)) {
    if (g4$nu > 1) { ESg4 <- g4$g_AB
    } else {
      ESg4 <- g4$delta_AB }
      CI4 <- c(ESg4 - g4$SE_g_AB*tlimit3 ,  ESg4 + g4$SE_g_AB*tlimit3)   # alpa = .05, df = 9
  }


  df <- data.frame(model = c("model1","model2","model3","model4","model5"),
                   Low_CI = round(c(CI0[1],CI1[1],CI2[1],CI3[1],CI4[1]),2),
                   ES_adj  = round(c(ES0,ESg1,ESg2,ESg3,ESg4),2),
                   Upp_CI = round(c(CI0[2],CI1[2],CI2[2],CI3[2],CI4[2]),2),
                   ES_unadj  = round(c(ES0,ES1,ES2,ES3,ES4),2))

  ### Add effect size
  res$output$effectSizes <- df

  ### Extract coefficients
  res$output$coef1 <- nlme::fixef(res$intermediate$lm.model1);
  res$output$coef2 <- nlme::fixef(res$intermediate$lm.model2);
  if (!is.null(g2)) {res$output$coef3 <- nlme::fixef(res$intermediate$lm.model3)
  } else {
    res$output$coef3 <- rep(NA,4)
    }
  if (!is.null(g3)) {res$output$coef4 <- nlme::fixef(res$intermediate$lm.model4)
  } else {
    res$output$coef4 <-  rep(NA,4)
  }
  if (!is.null(g4)) {res$output$coef5 <- nlme::fixef(res$intermediate$lm.model5)
  } else {
    res$output$coef5 <- rep(NA,4)
  }

  ### Add confidence intervals
  res$output$confint1 <- nlme::intervals(res$intermediate$lm.model1, which = "fixed");
  res$output$confint2 <- nlme::intervals(res$intermediate$lm.model2, which = "fixed");
  if (!is.null(g2)) {res$output$confint3 <- nlme::intervals(res$intermediate$lm.model3, which = "fixed")
  } else {
    res$output$confint3 <-  rep(NA,2)
  }
  if (!is.null(g3)) { res$output$confint4 <- nlme::intervals(res$intermediate$lm.model4, which = "fixed")
  } else {
    res$output$confint4 <-  rep(NA,2)
  }
  
  if (!is.null(g4)) { res$output$confint5 <- nlme::intervals(res$intermediate$lm.model5, which = "fixed")
  } else {
    res$output$confint5 <-  rep(NA,2)
  }
  
  ll3 <- ll4 <- ll5 <- NA
  if (!inherits(model3, "try-error"))  ll3 <- as.numeric(stats::logLik(res$intermediate$lm.model3)*-2)
  if (!inherits(model4, "try-error"))  ll4 <- as.numeric(stats::logLik(res$intermediate$lm.model4)*-2)
  if (!inherits(model5, "try-error"))  ll5 <- as.numeric(stats::logLik(res$intermediate$lm.model5)*-2)
  ### Add deviance and number of parameters
  res$output$deviance <- c(as.numeric(stats::logLik(res$intermediate$lm.model1)*-2),
                           as.numeric(stats::logLik(res$intermediate$lm.model2)*-2), ll3, ll4, ll5)
  names(res$output$deviance) <- c("deviance1","deviance2","deviance3","deviance4","deviance5")

  ndf3 <- ndf4 <- ndf5 <- NA
  if (!inherits(model3, "try-error"))  ndf3 <- attr(stats::logLik(res$intermediate$lm.model3),"df")
  if (!inherits(model4, "try-error"))  ndf4 <- attr(stats::logLik(res$intermediate$lm.model4),"df")
  if (!inherits(model5, "try-error"))  ndf5 <- attr(stats::logLik(res$intermediate$lm.model5),"df")
  res$output$numberOfParms <- c(attr(stats::logLik(res$intermediate$lm.model1),"df"),
                                attr(stats::logLik(res$intermediate$lm.model2),"df"),ndf3,ndf4, ndf5)

  cs3 <- cs4 <- cs5 <- NA
  if (!inherits(model3, "try-error"))  cs3 <- stats::coef(model3$modelStruct$corStruct, unconstrained = FALSE)
  if (!inherits(model4, "try-error"))  cs4 <- stats::coef(model4$modelStruct$corStruct, unconstrained = FALSE)
  if (!inherits(model5, "try-error"))  cs5 <- stats::coef(model5$modelStruct$corStruct, unconstrained = FALSE)
  
  res$output$autoCor  <- c(stats::coef(model2$modelStruct$corStruct, unconstrained = FALSE),cs4,cs5)

  names(res$output$autoCor) <- c("phi1","phi2","phi3")

  res$output$test1 <- stats::anova(model1, model2)
  res$output$test2 <- NULL

  if (!inherits(model4, "try-error") & (!inherits(model5, "try-error")))  {
     res$output$test2 <- stats::anova(model4, model5)
  }

  if (is.null(res$output$test2)) {
    if (inherits(model4, "try-error")) res$output$check <- "4"
    if (inherits(model5, "try-error")) res$output$check <- "5"
    if (inherits(model4, "try-error") & inherits(model4, "try-error")) res$output$check <- "4 and 5" }

  res$output$icc1 <- icc1


  class(res) <- 'pwMLRegr'
  return(res)

}



#' print.pwMLRegr
#'
#' This function prints the results of the pwMLRegr function
#' @method print pwMLRegr
#' @param x pwMLRegr object
#' @param ... not used
#' @return table with parameter estimates
#' @export
print.pwMLRegr <- function(x, ...) {

  et <- x$intermediate$evalTime
  alpha <- x$input$alpha

  cat ("Piecewise Multilevel Regression Analysis \n",
       "\nModels statistics:\n")

  cat ("Dependent variable: ", x$input$yVar  ,"\n")

  cat("\n  Effect sizes (delta's) at timepoint",et, "(model 3 and 4): \n",
      "\n  Model 1: Phase effect, no auto-correlation:      ", x$output$effectSize[1,3],
      "\n  Model 2: Phase effect, with auto-correlation:    ",  x$output$effectSize[2,3],
      "\n  Model 3: Piecewise regression:                   ",  x$output$effectSize[3,3],
      "\n  Model 4: Piecewise regression, auto-correlation: ", x$output$effectSize[4,3],
      "\n  Model 5: Piecewise regression, random time:      ", x$output$effectSize[4,3])

  cat("\n\n")
  cat("Note: All models with random intercept. \n\n\n")


  tablex <- data.frame(
    model = c("model1","model2","model3","model4","model5"),
    deviance = round(x$output$deviance, 0),
    parameters = round(x$output$numberOfParms, 0),
    autoCorr = c("no", round(x$output$autoCor, 3)[1],"no",round(x$output$autoCor, 3)[2:3]))

  tablex <- data.frame(cbind(tablex, x$output$effectSize[,-1]))

  row.names(tablex) <- NULL

  cap1 <- paste0("Estimates with ",100*(1-alpha),"% confidence interval")

  pander::pander(tablex, caption = cap1)

  cat("\n\nLikelihood ratio test auto-correlation:   \n\n")
  print(x$output$test1)

  if (is.null(x$output$test2)) {
    cat(paste0("\n\nNote: Model ",x$output$check," did not converge.\n\n"))
  } else {
    cat("\nLikelihood ratio test random time:    \n\n")
    print(x$output$test2)
  }

 return(tablex)

}
