
#' simPowerSCA
#'
#' Simulates Power for single case analyses, here the nonoverlap methods.
#' Based on effect size (ES) referring to a bivariate correlation, effect size due to a weekend effect (wES),
#' effect size due to an intervention (iES), number of assessments per day (nbeep) ,
#' number of days (ndays), and autocorrelation (ar)
#' alpha level can be corrected for number of tests (ntest) by Bonferroni correctie
#'
#' uses functions: lagESM or makeCorrelatedData
#'
#' @param H0result results from H0 simulation
#' @param nbeep number of assessments per day
#' @param ndays number of days
#' @param ES effect size referring to a bivariate correlation
#' @param iES effect size due to an intervention
#' @param phase0Perc percentage of the observations in first phase (default = 0.5)
#' @param corPredictors correlations between predictors
#' @param ar autocorrelation
#' @param artreat indicates if nothing ("none"), a prewhitening ("prewhitening"), or a lagged predictors ("dynamic") approach is used
#' @param nClose evaluate effect at the n closest points around intervention start
#' @param ntest correction for number of test (default = 1, no correction)
#' @param maxiter number of replications
#'
#' @importFrom stats arima.sim density lm na.exclude
#' quantile residuals rnorm time ts var sd
#' @return Mainly, this function computes the power estimates, it returns
#' an object containing three lists: \item{input}{The arguments specified
#' when calling the function} \item{intermediate}{Intermediat objects and
#' values} \item{output}{The results such as the power.}
#'
#' @author Peter Verboon (Open University of the Netherlands)
#'
#' Maintainer: Gjalt-Jorn Peters <gjalt-jorn@@userfriendlyscience.com>
#' @seealso \code{\link{genlog}}
#'
#'
#' @keywords single case analyses power
#' @rdname simPowerSCA
#' @examples
#' \dontrun{
#' res <- simPowerSCA(ndays = 50, ES = 0.40, iES = 0.50, phase0Perc = .40,
#' ar = 0.2, maxiter = 1000)
#' }
#' @export simPowerSCA
#'

simPowerSCA <- function(H0result = NULL, nbeep=1, ndays, ES = NULL, iES = NULL, maxiter=1000,
                        phase0Perc = NULL, corPredictors = NULL, ar = 0, artreat = "none", nClose = NULL, ntest=1) {

  if (!is.null(ES)) {
    if (abs(ES) > 1) return(cat("Input error: ES is a correlation coeffcient and should be between -1 and 1"))
  }
  alpha <- 0.05/ntest
  if (is.null(iES)) iES <- 0    # intervention effect
 # if (is.null(ES)) ES <- 0    # correlation effect
  if (is.null(phase0Perc)) phase0Perc <- 0.5
   x1 <- NULL

  ntot <- nbeep*ndays
  res0 <- matrix(data=0,nrow=maxiter, ncol=5)
  res1 <- matrix(data=0,nrow=maxiter, ncol=11)
  colnames(res0) <- c("intervention","regression","lagRegression","autocorL1","autocorL2" )
  colnames(res1) <- c("intervention","regression","lagRegression","autocorL1","autocorL2",
                      "Alpha10 Beta","Alpha05 Beta", "Alpha01 Beta",
                      "Alpha10 IV","Alpha05 IV", "Alpha01 IV")


  ## STEP 1. Create distribution under HO:  effect size = 0, autoregression and N are given.
  ##         Determine critical values corresponding with Type I errors: p < .10, p < 0.05 and p < 0.01
  ## STEP 2. Create distribution under H1:  autoregression, effect size and N are given
  ## STEP 3. Count how many times the results from STEP 2 are larger than critical values from STEP 1 (is power estimate).


  # define critical values
  cv99 <-  as.numeric(round(.995*maxiter))
  cv95 <-  as.numeric(round(.975*maxiter))
  cv90 <-  as.numeric(round(.950*maxiter))

  cd <- rep(NA, maxiter)

  ## construct beepnumbers and daynumbers and select weekend
  beepnr <- rep(seq(1:nbeep),ndays)
  daynr <- sort(rep(seq(1:ndays),nbeep))
  phase <- c(rep(FALSE, phase0Perc*ntot),rep(TRUE, (1-phase0Perc)*ntot))
  if ((ntot - length(phase)) > 0 ) phase <- c(phase, TRUE)

  if (ar == 0) ar <- .0001   # zero value yields error in arima.sim function

  ## STEP 1

  if (is.null(H0result)) {

    for (i in 1:maxiter) {

      y <- arima.sim(list(order = c(1,0,0), ar = ar), n = ntot, sd = 1)        # An auto-correlation simulation

      intervention <- phase*1

      # add variable(s) with given correlation
      if (!is.null(ES)) {
        dat0 <- makeCorrelatedData(fixedVar = y, cor1 = 0, cor2 = corPredictors)
        x1 <- dat0[,2]
        }
      dat1 <- data.frame(cbind(intervention, y, x1))

      # Add lagged variables to a new data set, called "dat1"


      plus <- ifelse(is.null(x1), " ",  " + ")
      xpred <- ifelse(is.null(x1), " ",  " x1 ")
      xpredL1 <- ifelse(is.null(x1), " ",  " x1L1 ")

       if (!is.null(x1))  dat1$x1L1 <- c(NA,dat1$x1)[-(length(dat1$x1)+1)]

      # Optionally remove auto-correlation by pre-withening (lag 1 and 2),
      # then run analysis and store the results
      if (artreat == "dynamic") {
        dat1$yL1 <- c(NA,dat1$y)[-(length(dat1$y)+1)]
        dat1$yL2 <- c(NA,NA,dat1$y)[-(length(dat1$y)+1:2)]
        ff <- stats::as.formula(paste0("y ~ intervention ", plus, xpred, plus, xpredL1,"+ yL1 + yL2"))
        fit <- lm(formula = ff, data = dat1)
      }
      if (artreat == "prewhitening") {
        dat1$yL1 <- c(NA,dat1$y)[-(length(dat1$y)+1)]
        dat1$yL2 <- c(NA,NA,dat1$y)[-(length(dat1$y)+1:2)]
        dat1$res <-  residuals(lm(dat1$y ~ dat1$yL1 + dat1$yL2 , na.action=na.exclude))
        ff <- stats::as.formula(paste0("res ~  intervention ",plus, xpred, plus, xpredL1))
        fit <- lm(formula = ff, data = dat1)
      }
      if (artreat == "none") {
        ff <- stats::as.formula(paste0("y ~  intervention ",plus, xpred, plus, xpredL1))
        fit <- lm(formula = ff, data = dat1)
      }


      res0[i,1] <- fit$coefficients[2]                                          # intervention
      res0[i,2] <- ifelse(is.null(x1), NA, fit$coefficients[3] )                # regression
      res0[i,3] <- ifelse(is.null(x1), NA, fit$coefficients[4] )                # lagged regression
      res0[i,4] <- ifelse(artreat == "dynamic",fit$coefficients[-2], NA)         # auto-correlation lag1
      res0[i,5] <- ifelse(artreat == "dynamic",fit$coefficients[-1], NA)         # auto-correlation lag2
    }

    # order the results
    res0 <- data.frame(res0)
    res0$regression <- res0[order(res0[,1]),1]
    res0$intervention <- res0[order(res0[,2]),2]
    res0$lagRegression <- res0[order(res0[,3]),3]
    res0$autocorL1 <- res0[order(res0[,3]),4]
    res0$autocorL2 <- res0[order(res0[,3]),5]


    # compute critical values under H0
    critValuesRegression <- if (!is.null(x1)) quantile(res0[,"regression"], c(.95, .975, .995))
    critValuesIntervention <- quantile(res0[,"intervention"], c(.95, .975, .995))

  } else {

    res0 <- H0result$H0
    critValuesRegression <- H0result$critValuesRegression
    critValuesIntervention <- H0result$critValuesIntervention

  }



  ## STEP 2

  for (i in 1:maxiter) {

    y <- arima.sim(list(order = c(1,0,0), ar = ar), n = ntot, sd = 1)         # An autocorrelation simulation
    intervention <- phase*1

    # add variable(s) with given correlation
    if (!is.null(ES)) {
      dat0 <- makeCorrelatedData(fixedVar = y, cor1 = ES, cor2 = corPredictors)
      x1 <- dat0[,2]
    }
    dat1 <- data.frame(cbind(intervention, y, x1))

    dat1$y[phase] <- dat1$y[phase] + rnorm(sum(phase), iES, 1)                      # add intervention effect size


    # Optionally select only nClose observations around level change moment
    if (!is.null(nClose)) {
      split <- table(dat1$intervention)[1]
      dat1 <- dat1[((split-nClose):(split+nClose)),]
    }

    cd[i] <- (ufs::computeEffectSize_d(dat1$y, dat1$intervention))$es

    plus <- ifelse(is.null(x1), " ",  " + ")
    xpred <- ifelse(is.null(x1), " ",  " x1 ")
    xpredL1 <- ifelse(is.null(x1), " ",  " x1L1 ")

    if (!is.null(x1))  x1L1 <- c(NA,dat1$x1)[-(length(dat1$x1)+1)]

    # Optionally remove auto-correlation by pre-withening (lag 1 and 2),
    # then run analysis and store the results
    if (artreat == "dynamic") {
      dat1$yL1 <- c(NA,dat1$y)[-(length(dat1$y)+1)]
      dat1$yL2 <- c(NA,NA,dat1$y)[-(length(dat1$y)+1:2)]
      ff <- stats::as.formula(paste0("y ~ intervention ", plus, xpred, plus, xpredL1,"+ yL1 + yL2"))
      fit <- lm(formula = ff, data = dat1)
    }
    if (artreat == "prewhitening") {
      dat1$yL1 <- c(NA,dat1$y)[-(length(dat1$y)+1)]
      dat1$yL2 <- c(NA,NA,dat1$y)[-(length(dat1$y)+1:2)]
      dat1$res <-  residuals(lm(dat1$y ~ dat1$yL1 + dat1$yL2 , na.action=na.exclude))
      ff <- stats::as.formula(paste0("res ~  intervention ",plus, xpred, plus, xpredL1))
      fit <- lm(formula = ff, data = dat1)
    }
    if (artreat == "none") {
      ff <- stats::as.formula(paste0("y ~  intervention ",plus, xpred, plus, xpredL1))
      fit <- lm(formula = ff, data = dat1)
    }



    res1[i,1] <- fit$coefficients[2]                                          # intervention
    res1[i,2] <- ifelse(is.null(x1), NA, fit$coefficients[3] )                # regression
    res1[i,3] <- ifelse(is.null(x1), NA, fit$coefficients[4] )                # lagged regression
    res1[i,4] <- ifelse(artreat == "dynamic",fit$coefficients[-2], NA)         # auto-correlation lag1
    res1[i,5] <- ifelse(artreat == "dynamic",fit$coefficients[-1], NA)         # auto-correlation lag2
    if (!is.null(x1)) res1[i,6:8] <- res1[i,1] >= critValuesRegression
    res1[i,9:11] <- res1[i,"intervention"] >= critValuesIntervention

  }

  res0 <- data.frame(res0)
  res1 <- data.frame(res1)

  result <- list(H0 = res0,
                 H1 = res1,
                 cohensD = cd,
                 critValuesRegression = critValuesRegression,
                 critValuesIntervention = critValuesIntervention)

  return(result)


}  # END FUNCTION

