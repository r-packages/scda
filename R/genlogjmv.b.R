
# This file is a generated template, your changes will not be overwritten

genlogjmvClass <- if (requireNamespace('jmvcore')) R6::R6Class(
    "genlogjmvClass",
    inherit = genlogjmvBase,
    private = list(
        .run = function() {

            data <- self$data

            ready <- !is.null(self$options$dep) &&
                !(is.null(self$options$phase) && is.null(self$options$baseNumber)) &&
                !(is.null(self$options$phase) && (self$options$baseNumber == 0))

            ready <- ready && !is.null(self$options$time)



            if (ready == TRUE) {

                outgl <- scd::genlog(
                    data =  self$data,
                    yVar  = self$options$dep,
                    phaseVar = self$options$phase,
                    timeVar = self$options$time,
                    baselineMeasurements = self$options$baseNumber)

                self$results$text$setContent(outgl$output)

                inflectionPoint <- round(outgl$output$inflectionPoint,3)
                growthRate <- round(outgl$output$growthRate, 3)
                base <- round(outgl$output$base, 3)
                top <- round(outgl$output$top, 3)

                df <- data.frame(name = c("inflectionPoint","growthRate","base","top"),
                                 est = c(inflectionPoint,growthRate,base,top))
                private$.populateGLTable(df)

                deviance <- round(outgl$output$deviance,3)
                Rsq <- round(outgl$output$Rsq, 3)
                ESc <- round(outgl$output$ESc, 3)
                ESr <- round(outgl$output$ESr, 3)

                df2 <- data.frame(
                    statistic = c("deviance","R-squared","Effect Size Cohen","Effect Size Range"),
                    est = c(deviance,Rsq,ESc,ESr))

                private$.populateGL2Table(df2)


                image <- self$results$plotData     # empty plot defined in r.yaml
                image$setState(outgl)                 # construct plot

            } # ready
        },


        .populateGLTable = function(df) {


            table <- self$results$genlogParameters           # empty table, defined in r.yaml

            for (rowNo in 1:4) {
                table$setRow(rowNo=rowNo,
                             values=list(name = c("inflectionPoint","growthRate","base","top")[rowNo],
                                         est = df[rowNo, "est"] )
                )
            }
        }, # end populateGLTable




        .populateGL2Table = function(df) {


            table <- self$results$genlogFit           # empty table, defined in r.yaml

            for (rowNo in 1:4) {
                table$setRow(rowNo=rowNo,
                             values=list(statistic = df[rowNo, "statistic"],
                                         est = df[rowNo, "est"] )
                )
            }
        }, # end populateGL2Table





        .plotData = function(image, ...) {     # Function name corresponds to r.yaml definition
            if (is.null(image$state) || is.na(image$state)) return(FALSE)
            x <- image$state
            plot <- x$output$plot
            print(plot)
            TRUE

        }




    )
)

