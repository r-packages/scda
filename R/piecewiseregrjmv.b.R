
# This file is a generated template, your changes will not be overwritten

piecewiseregrjmvClass <- if (requireNamespace('jmvcore')) R6::R6Class(
    "piecewiseregrjmvClass",
    inherit = piecewiseregrjmvBase,
    private = list(
        .run = function() {

            data <- self$data

            ready <- !is.null(self$options$dep) &&
                !(is.null(self$options$phase) && is.null(self$options$baseNumber)) &&
                !(is.null(self$options$phase) && (self$options$baseNumber == 0))

            ready <- ready && !is.null(self$options$time)

            if ((!is.null(self$options$phase)) &&
                length(unique(data[,self$options$phase])) > 2) {
                    text <- "Error: phase variable must have exactly two values"
                    self$results$text$setContent(text)
                    ready <- FALSE
            }


            self$results$text$setContent(ready)

            if (ready == TRUE) {

            outpwr <- piecewiseRegr(
                dat =  self$data,
                yVar  = self$options$dep,
                phaseVar = self$options$phase,
                timeVar = self$options$time,
                baselineMeasurements = self$options$baseNumber)

            private$.populatePwrTable(outpwr)

            image <- self$results$plotData     # empty plot defined in r.yaml
            image$setState(outpwr)                 # construct plot


            } # ready


        },


        .populatePwrTable = function(res) {

            est <- res$output$coef
            cis <- res$output$confint
            df <- round(as.data.frame(cbind(est, cis)),3)
            df$name <- c("intercept","level change","initial trend" ,"trend change")

            table <- self$results$pwr           # empty table, defined in r.yaml

            for (rowNo in 1:4) {
                table$setRow(rowNo=rowNo, values=list(
                    name = df[rowNo, "name"],
                    est = df[rowNo, "est"],
                    CI_25    = df[rowNo, "2.5 %"],
                    CI_975   = df[rowNo, "97.5 %"]
                )
                )
            }


        },  # end populate Pwr function





        .plotData = function(image, ...) {     # Function name corresponds to r.yaml definition
            if (is.null(image$state) || is.na(image$state)) return(FALSE)
            res <- image$state

            colors <- list(pre = viridis::viridis(4)[1],
                           post = viridis::viridis(4)[4],
                           diff = viridis::viridis(4)[3],
                           intervention = viridis::viridis(4)[2],
                           points = "black")
            theme <- ggplot2::theme_minimal()
            pointSize <- 2
            pointAlpha <- 1
            lineSize <- 1
            yRange <- NULL
            yBreaks <-  NULL
            showPlot <- TRUE
            plotLabs <- NULL


            data <- as.data.frame(res$intermediate$dat)
            nA <- res$intermediate$baselineMeasurements
            yVar <-  res$input$yVar
            timeVar <- res$input$timeVar
            phaseVar <- res$input$phaseVar

            if (is.null(phaseVar))  phaseVar <- 'phaseVar'

            if (is.factor(data[, phaseVar])) {
                phaseVarMin <- min(levels(data[, phaseVar]));
                phaseVarMax <- max(levels(data[, phaseVar]));
            } else {
                phaseVarMin <- min(data[, phaseVar]);
                phaseVarMax <- max(data[, phaseVar]);
            }


            # plot <- ggplot2::ggplot(data, ggplot2::aes(x = data[,timeVar], y = data[,yVar])) +
            #     ggplot2::geom_point()

            ypre <- res$output$coef[1] + res$output$coef[3] * data[, timeVar];
            ypost <- res$intermediate$lm.model$fitted.values;
            predictionDf1 <- data.frame(x = data[nA, timeVar],
                                        xend = data[nA+1, timeVar],
                                        y = ypre[nA],
                                        yend = ypre[nA+1]);
            predictionDf2 <- data.frame(x = data[nA+1, timeVar],
                                        xend = data[nA+1, timeVar],
                                        y = ypre[nA+1],
                                        yend = ypost[nA+1]);

            if (!is.null(res$intermediate$day0)) {
                data[, timeVar] <-
                    as.POSIXct(data[, timeVar], origin = res$intermediate$day0);
                predictionDf1$x <-
                    as.POSIXct(predictionDf1$x, origin = res$intermediate$day0);
                predictionDf1$xend <-
                    as.POSIXct(predictionDf1$xend, origin = res$intermediate$day0);
                predictionDf2$x <-
                    as.POSIXct(predictionDf2$x, origin = res$intermediate$day0);
                predictionDf2$xend <-
                    as.POSIXct(predictionDf2$xend, origin = res$intermediate$day0);
            }

            if (is.null(plotLabs)) {
                plotLabs <- list(x = ifelse(is.null(res$intermediate$day0.formatted),
                                            "Measurements",
                                            "Date"),
                                 #paste0("Days since ", res$intermediate$day0.formatted)),
                                 y = yVar);
            }

            if (is.null(yRange)) {
                yRange <- range(data[, yVar], na.rm=TRUE);
            }

            if (is.null(yBreaks)) {
                yBreaks <- pretty(c(yRange, data[, yVar]),
                                  n=(floor(max(yRange) - min(yRange))));
            } else {
                yBreaks <- seq(from = floor(min(yRange)),
                               to = ceiling(max(yRange)),
                               by = yBreaks)
            }


           plot <-
                ggplot2::ggplot(data = data,
                                ggplot2::aes_string(x = timeVar,
                                                    y = yVar)) +
                ggplot2::geom_vline(
                    xintercept = mean(c(data[nA, timeVar],
                                        data[nA + 1, timeVar])),
                    colour = colors$intervention,
                    size = lineSize
                ) +
                ggplot2::geom_smooth(
                    data = data[data[, phaseVar] == phaseVarMin,],
                    method = 'lm',
                    color = colors$pre,
                    fill = colors$pre,
                    size = lineSize
                ) +
                ggplot2::geom_smooth(
                    data = data[data[, phaseVar] == phaseVarMax,],
                    method = 'lm',
                    color = colors$post,
                    fill = colors$post,
                    size = lineSize
                ) +
                ggplot2::geom_segment(
                    data = predictionDf1,
                    ggplot2::aes_string(
                        x = 'x',
                        xend = 'xend',
                        y = 'y',
                        yend = 'yend'
                    ),
                    color = colors$pre,
                    size = lineSize,
                    linetype = 'dotted'
                ) +
                ggplot2::geom_segment(
                    data = predictionDf2,
                    ggplot2::aes_string(
                        x = 'x',
                        xend = 'xend',
                        y = 'y',
                        yend = 'yend'
                    ),
                    color = colors$diff,
                    size = lineSize
                ) +
                ggplot2::geom_point(size = pointSize,
                                    alpha = pointAlpha,
                                    color = colors$points) +
                ggplot2::scale_y_continuous(breaks = yBreaks) +
                ggplot2::coord_cartesian(ylim = yRange) +
                theme +
                do.call(ggplot2::labs, plotLabs)

            if (!is.null(res$intermediate$day0)) {
                res$output$plot <-
                    res$output$plot + ggplot2::scale_x_datetime(date_breaks="2 months",
                                                                date_labels="%m-%Y");
            }


            print(plot)
            TRUE


        } # end plot pwr function

    )
)
