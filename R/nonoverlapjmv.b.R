
# This file is a generated template, your changes will not be overwritten

nonOverlapjmvClass <- if (requireNamespace('jmvcore')) R6::R6Class(
    "nonOverlapjmvClass",
    inherit = nonOverlapjmvBase,
    private = list(
        .run = function() {

            # if (self$results$isFilled()) return()


            ready <- !is.null(self$options$dep) &&
                !(is.null(self$options$phase) && is.null(self$options$baseNumber)) &&
                !(is.null(self$options$phase) && (self$options$baseNumber == 0))

            data <- self$data

            if (!is.null(self$options$dep)) {
               if (!is.numeric(data[,self$options$dep])) {
                text <- "Error: dependent variable must be numeric"
                self$results$text$setContent(text)
                ready <- FALSE
                }
            }

            if (ready == TRUE) {


                r1 <- nonOverlap(
                        dat =  self$data,
                        yVar  = self$options$dep,
                        phaseVar = self$options$phase,
                        baselineMeasurements = self$options$baseNumber,
                        improvement = self$options$improvement)

                    df <- r1$output$results

                    if (self$options$randomization) {

                        if (self$options$minPhase > (0.3*(dim(self$data)[1]))) {
                             self$results$text$setContent("Parameter minPhase is too large")
                             return()
                        }

                        r2 <- permutationTest(
                            dat =  self$data,
                            yVar  = self$options$dep,
                            phaseVar = self$options$phase,
                            baselineMeasurements = self$options$baseNumber,
                            minPhase = self$options$minPhase,
                            improvement = self$options$improvement)

                        df$pvalue <- r2$output$pvalues

                    }



                    private$.populateNoLapTable(df)


                    image <- self$results$plotData     # empty plot defined in r.yaml
                    image$setState(r1)                 # construct plot





            }      # ready


        },         # end run



        .populateNoLapTable = function(res) {

            table <- self$results$nonOverlap           # empty table, defined in r.yaml

            for (rowNo in 1:7) {
                table$setRow(rowNo=rowNo,
                             values=list(method = res[rowNo, 1],
                                         est     = res[rowNo, "statistic"],
                                         pvalue   = res[rowNo, "pvalue"]));
            }


        },  # end populate NonOverLap function






        .plotData = function(image, ...) {     # Function name corresponds to r.yaml definition
            if (is.null(image$state) || is.na(image$state)) return(FALSE)
            x <- image$state


            df <- x$intermediate$data
            yVar <-  x$input$yVar
            timeVar <- x$input$timeVar
            phaseVar <- x$input$phaseVar
            Npre <- x$intermediate$Npre
            N <- dim(df)[1]

            if (!is.null(phaseVar)) {
                 df[,(x$input$phaseVar)] <-  as.factor(df[,(x$input$phaseVar)])
            } else {
                 phaseVar <- "phaseVar"
                 df$phaseVar <- as.factor(c(rep(0,Npre), rep(1, (N - Npre) )))
            }
            improvement <- x$input$improvement

            plot <-   ggplot2::ggplot(data = df, ggplot2::aes_string(x = df$timeVar, y = yVar, colour = phaseVar)) +
                ggplot2::geom_point() + ggplot2::theme_bw() +
                ggplot2::xlab("measurements") +
                ggplot2::geom_vline(xintercept = mean(c(df[Npre, timeVar], df[Npre + 1, timeVar])))


            if (improvement == "increase") {
                plot <- plot + ggplot2::geom_hline(yintercept=max(df[1:Npre,yVar]), color = "black", size = .2)
                plot <- plot + ggplot2::geom_hline(yintercept=min(df[(Npre+1):nrow(df),yVar]), color = "black", size = .2)

                plot <- plot + ggplot2::geom_ribbon(ggplot2::aes(ymin=min(df[(Npre+1):nrow(df),yVar]),
                                                                 ymax=max(df[1:Npre,yVar])), fill = "grey70", alpha = .50)
            }
            if (improvement == "decrease") {
                plot <- plot + ggplot2::geom_hline(yintercept=min(df[1:Npre,yVar]), color = "black", size = .2)
                plot <- plot + ggplot2::geom_hline(yintercept=max(df[(Npre+1):nrow(df),yVar]), color = "black", size = .2)

                plot <- plot + ggplot2::geom_ribbon(ggplot2::aes(ymin=min(df[(1:Npre),yVar]),
                                                                 ymax=max(df[(Npre+1):nrow(df),yVar])), fill = "grey70", alpha = .50)
            }



            print(plot)
            TRUE;

        } # end plot Data





    ) # end list private
) #end

