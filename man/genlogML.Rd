% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/genlogML.R
\name{genlogML}
\alias{genlogML}
\title{Generalized Logistic Multilevel Analysis}
\usage{
genlogML(
  data,
  timeVar,
  yVar,
  groupVar = NULL,
  yRange = NULL,
  randomPar = NULL,
  fixedPar = NULL,
  startInflection = NULL,
  startGrowthRate = NULL,
  maxiter = NULL
)
}
\arguments{
\item{data}{The dataframe containing the variables for the analysis.}

\item{timeVar}{The name of the variable containing the measurement moments
(or an index of measurement moments).}

\item{yVar}{The name of the dependent variable.
that this normally should only have two possible values.}

\item{groupVar}{identifier of the group level (e.g. subject numbers)}

\item{yRange}{This can be used to manually specify the possible values that
the dependent variable can take.
are specified, the range of the dependent variable is used instead.}

\item{randomPar}{the random terms, either inflection point ("ip"), or growth rate ("gt")
or "both" (the default)}

\item{fixedPar}{this option is not yet active: inflection point and growth parameter are the default}

\item{startInflection}{optional start value for inflection point parameter}

\item{startGrowthRate}{optional start value for growth rate  parameter}

\item{maxiter}{The maximum number of iterations used}
}
\value{
Mainly, this function returns an object
object containing three lists: \item{input}{The arguments specified
when calling the function} \item{intermediate}{Intermediate objects and
values} \item{output}{The results of the analysis}
Print and plot methods are available for this object.
}
\description{
This function implements the generalized logistic multilevel analysis. The single level variant
was introduced in Verboon & Peters (2020). This analysis fits a logistic function (i.e. a
sigmoid) to a data series. The present function is  actually a wrapper around the nlme function from
the nlme package.
}
\examples{

### Load dataset
data(Singh);

### Conduct generalized logistic multilevel analysis
scda::genlogML(
  Singh,
  timeVar='time',
  yVar='score_verbal',
  groupVar = 'tier',
  randomPar = 'ip',
  startGrowthRate = -2
);

}
\references{
Verboon, P. & Peters, G.-J. Y. (2020) Applying the Generalized
Logistic Model in Single Case Designs: Modeling Treatment-Induced Shifts.
\emph{Behavior Modification, 44(1)}
\doi{10.1177/0145445518791255}
}
\author{
Peter Verboon (Open University of the Netherlands)

Maintainer: Peter Verboon \href{mailto:peter.verboon@ou.nl}{peter.verboon@ou.nl}
}
\keyword{case}
\keyword{curve}
\keyword{logistic}
\keyword{models}
\keyword{single}
