% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/NonoverlapFunctions.R
\name{NAP}
\alias{NAP}
\title{Non-overlap of all pairs}
\usage{
NAP(
  score,
  n_a,
  improvement = "increase",
  SE = TRUE,
  CI = TRUE,
  confidence = 0.95
)
}
\arguments{
\item{score}{vector of numeric data for A and B phase. Missing values are dropped.}

\item{n_a}{number of observations in Phase A.}

\item{improvement}{character string indicating direction of improvement.
Default is "increase"}

\item{SE}{logical value indicating whether to report the standard error}

\item{CI}{logical value indicating whether to report a confidence interval}

\item{confidence}{confidence level for the reported interval estimate}
}
\value{
A list containing the estimate, standard error, and/or confidence
interval.
}
\description{
Calculates the non-overlap of all pairs index (Parker & Vannest,
2009).
}
\details{
NAP is calculated as the proportion of all pairs of one observation
from each phase in which the measurement from the B phase improves upon the
measurement from the A phase, with pairs of data points that are exactly
tied being given a weight of 0.5. The range of NAP is {0,1}, with a null
value of 0.5.

The standard error of NAP is calculated based on the method of Hanley and
McNeil (1982).

The confidence interval for NAP is calculated based on the symmetrized
score-inversion method (Method 5) proposed by Newcombe (2006).
}
\examples{
A <- c(20, 20, 26, 25, 22, 23)
B <- c(28, 25, 24, 27, 30, 30, 29)
AB <- c(A,B)
NAP(score = AB, n_a = 6)

# Example from Parker & Vannest (2009)
yA <- c(4, 3, 4, 3, 4, 7, 5, 2, 3, 2)
yB <- c(5, 9, 7, 9, 7, 5, 9, 11, 11, 10, 9)
yAB <- c(yA,yB)
NAP(score = yAB, n_a = 6)

}
\references{
Hanley, J. A., & McNeil, B. J. (1982). The meaning and use of the area under
a receiver operating characteristic (ROC) curve. \emph{Radiology, 143},
29--36.
\doi{10.1148/radiology.143.1.7063747}

Newcombe, R. G. (2006). Confidence intervals for an effect size measure based
on the Mann-Whitney statistic. Part 2: Asymptotic methods and evaluation.
\emph{Statistics in Medicine, 25}(4), 559--573.
\doi{10.1002/sim.2324}

Parker, R. I., & Vannest, K. J. (2009). An improved effect size for
single-case research: Nonoverlap of all pairs. \emph{Behavior Therapy,
40}(4), 357--67.
\doi{10.1016/j.beth.2008.10.006}
}
