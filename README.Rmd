---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r setup, include=FALSE}

###-----------------------------------------------------------------------------
### This Readme file is a generic template that automatically creates a minimal
### readme file. You only have to change the following two lines, and then in
### the rest of the file, the bits in between the comment blocks marking the
### custom bits for every package. The general info will be generated
### automatically.
###-----------------------------------------------------------------------------

packagename <- 'scda';
packageSubtitle <- "Single Case Designs Analysis";

###-----------------------------------------------------------------------------

knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)

gitLab_ci_badge <-
  paste0("https://gitlab.com/r-packages/", packagename, "/badges/master/pipeline.svg");
gitLab_ci_url <-
  paste0("https://gitlab.com/r-packages/", packagename, "/commits/master");

codecov_badge <-
  paste0("https://codecov.io/gl/r-packages/", packagename, "/branch/master/graph/badge.svg");
codecov_url <-
  paste0("https://codecov.io/gl/r-packages/", packagename, "?branch=master");

dependency_badge <-
  paste0("https://tinyverse.netlify.com/badge/", packagename);
dependency_url <-
  paste0("https://CRAN.R-project.org/package=", packagename);

pkgdown_url <-
  paste0("https://r-packages.gitlab.io/", packagename);

hexlogo <- ifelse(file.exists("img/hex-logo.png"),
                  "<img src='img/hex-logo.png' align=\"right\" height=\"200\" /> ",
                  "");

```

# `r paste0(hexlogo, packagename, " \U1F4E6")`

## `r packageSubtitle`

<!-- badges: start -->

[![Pipeline status](`r gitLab_ci_badge`)](`r gitLab_ci_url`)

[![Coverage status](`r codecov_badge`)](`r codecov_url`)

<!-- [![Dependency status](`r dependency_badge`)](`r dependency_url`) -->

<!-- badges: end -->

The pkgdown website for this project is located at `r pkgdown_url`.

<!--------------------------------------------->
<!-- Start of a custom bit for every package -->
<!--------------------------------------------->

The goal of `scda` is to combine a number of functions for the analysis of single case designs (also known as n-of-1 designs).

<!--------------------------------------------->
<!--  End of a custom bit for every package  -->
<!--------------------------------------------->

## Installation

You can install the released version of ``r packagename`` from [CRAN](https://CRAN.R-project.org) with:

```{r echo=FALSE, comment="", results="asis"}
cat(paste0("``` r
install.packages('", packagename, "');
```"));
```

You can install the development version of ``r packagename`` from [GitLab](https://gitlab.com) with:

```{r echo=FALSE, comment="", results="asis"}
cat(paste0("``` r
remotes::install_gitlab('r-packages/", packagename, "');
```"))
```

(assuming you have `remotes` installed; otherwise, install that first using the `install.packages` function)

<!--------------------------------------------->
<!-- Start of a custom bit for every package -->
<!--------------------------------------------->

<!--------------------------------------------->
<!--  End of a custom bit for every package  -->
<!--------------------------------------------->
