
<!-- README.md is generated from README.Rmd. Please edit that file -->

# scda 📦

## Single Case Designs Analysis

<!-- badges: start -->

[![Pipeline
status](https://gitlab.com/r-packages/scda/badges/master/pipeline.svg)](https://gitlab.com/r-packages/scda/commits/master)

[![Coverage
status](https://codecov.io/gl/r-packages/scda/branch/master/graph/badge.svg)](https://codecov.io/gl/r-packages/scda?branch=master)

<!-- [![Dependency status](https://tinyverse.netlify.com/badge/scda)](https://CRAN.R-project.org/package=scda) -->
<!-- badges: end -->

The pkgdown website for this project is located at
<https://r-packages.gitlab.io/scda>.

<!--------------------------------------------->
<!-- Start of a custom bit for every package -->
<!--------------------------------------------->

The goal of `scda` is to combine a number of functions for the analysis
of single case designs (also known as n-of-1 designs).

<!--------------------------------------------->
<!--  End of a custom bit for every package  -->
<!--------------------------------------------->

## Installation

You can install the released version of `scda` from
[CRAN](https://CRAN.R-project.org) with:

``` r
install.packages('scda');
```

You can install the development version of `scda` from
[GitLab](https://gitlab.com) with:

``` r
remotes::install_gitlab('r-packages/scda');
```

(assuming you have `remotes` installed; otherwise, install that first
using the `install.packages` function)

<!--------------------------------------------->
<!-- Start of a custom bit for every package -->
<!--------------------------------------------->
<!--------------------------------------------->
<!--  End of a custom bit for every package  -->
<!--------------------------------------------->
